#!/usr/bin/env python3
#

import sys
import math
import time

import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio

# fealpy module
from fealpy.functionspace import FourierSpace
from fealpy.timeintegratoralg.timeline import UniformTimeLine

# scftt module
from GCSCFTA1B1A2B2LinearModel import GCSCFTA1B1A2B2LinearModel, model_options, init_value

class SCFTA1BA2CLinearModelTest():

    def __init__(self):
        pass

    def init_value(self):
        NS = 32
        box = np.array([[0.71, 0, 0], [0, 0.71, 0], [0, 0, 0.71]], dtype=np.float)
        space = FourierSpace(box,  NS)
        rhoA = init_value['fcc']
        rhoA = space.fourier_interpolation(rhoA)
        rhoB = 1-rhoA

        print('rhoA:', rhoA)
        print('rhoB:', rhoB)

        import scipy.io as sio

        data = {
                'rhoA':rhoA,
                'rhoB':rhoB,
                }
        sio.savemat('rho0.mat', data)


    def run(self, phase):
        if phase == 'bcc':
            NS = 64
            L = 5.73
            box = np.array([[L, 0, 0], [0, L, 0], [0, 0, L]], dtype=np.float)
            data = sio.loadmat('./BCC/BCC-init.mat')
            wA = data['W'][...,0].reshape(NS,NS,NS)
            wB = data['W'][...,1].reshape(NS,NS,NS)
            w = np.array([wA, wB])
        elif phase =='hcp':
            NS = 96
            box = np.array([[5, 0, 0], [0, 8.66, 0], [0, 0, 8.14]], dtype=np.float)
            data = sio.loadmat('HCP.mat')
            wA = data['wA'].reshape(NS,NS,NS)
            wB = data['wB'].reshape(NS,NS,NS)
            w = np.array([wA, wB])
        elif phase =='sigma':
            NS = np.array([128,128,64])
            box = np.array([[17.77, 0, 0], [0, 17.77, 0], [0, 0, 9.39]], dtype=np.float)
            data = sio.loadmat('HCP.mat')
            wA = data['wA']
            wB = data['wB']
            w = np.array([wA, wB])
        elif phase == 'hex':
            NS = 256
            #L = 8
            box = np.array([[4.64, 0], [0, 4]], dtype=np.float)
            data = sio.loadmat('ABAB/HEX/hex.mat')
            wA = data['wA'].reshape(NS,NS)
            wB = data['wB'].reshape(NS,NS)
            w = np.array([wA, wB])
            #w = data['w']


        fA1  = 0.2
        fA2 = 0.3
        gamma = 1.5
        chiAB = 0.25###改变chiAB
        for e_mu in np.arange(0.3,1.1,0.1):
            print('chiAB', chiAB,e_mu)
            options = model_options(box=box, NS=NS, fA1=fA1, fA2=fA2, chiAB=chiAB,
                    gamma=gamma, e_mu=e_mu)
            model = GCSCFTA1B1A2B2LinearModel(options=options)
            model.init_field(w)
            H = np.inf
            H_diff = np.inf
            e_diff = 1
            maxit = options['Maxit']
            if True:
                for i in range(maxit):
                    print("step:", i)
                    start =time.clock()
                    model.compute(e_diff = 1)
                    print('H:',H)
                    end =time.clock()
                    print('Running time: %s Seconds'%(end-start))
                    H_diff_new = np.abs(H - model.H)
                    e_diff = H_diff - H_diff_new
                    H_diff = H_diff_new
                    print('Hmodel:', model.H)
                    print('Hdiff:',H_diff)
                    H = model.H
                    ng = list(map(model.space.function_norm, model.grad))
                    print("l2 norm of grad:", ng)
                    if H_diff < options['tol']:
                        if np.max(model.rho[:,0]) < 1 and np.min(model.rho[:,0]) >0:
                            break
                model.save_data('./HEX/HEXchiAB'+str(chiAB)+'mu'+str(e_mu)+'.mat')
    

test = SCFTA1BA2CLinearModelTest()
if sys.argv[1] == "init_value":
    test.init_value()
elif sys.argv[1] == "run":
    test.run(phase = sys.argv[2])
