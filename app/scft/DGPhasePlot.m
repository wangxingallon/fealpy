%     load 'DG[0.1000--2.0000-2.0000][BBS][64-64-64][0.1000]103'
  i_0=1;
% u1=u_1{i_0};
% u2=u_2{i_0};
 rhocr1=rhocr_1{i_0};
 rhoRr3=rhoRr_3{i_0};
% u1=real(ifftn(mymap(fftn(u1),zeros(300,300,300),'cs2rf')));
%  u2=real(ifftn(mymap(fftn(u2),zeros(300,300,300),'cs2rf')));
%  rhocr1=real(ifftn(mymap(fftn(rhocr1),zeros(300,300,300),'cs2rf')));
%  B=zeros(48,48,48);
%  rhoRr=real(ifftn(mymap(fftn(rhoRr3)/numel(rhoRr3),B,'cs2rf')))*numel(B);
%   rhoRr3=real(ifftn(mymap(fftn(rhoRr3),zeros(300,300,300),'cs2rf')));    
close all;
set(0, 'DefaultFigureVisible', 'on');

figure(8) 
    
%      ur0 =rhocr1;
     ur0=rhoRr3;
    reg = dirBox;
    ncpt = size(ur0);
%     ur0 = real(fftn(ur0));

    Size = [0,0,6,6];
    %figure('Name', 'Structure', 'Position', 150*Size);
	set(gcf, 'color', 'white')
	minu = min(ur0(:));
	maxu = max(ur0(:));
	isoA = minu+0.9*(maxu-minu);
x=linspace(0, reg(1,1), ncpt(1));
	y=linspace(0, reg(2,2), ncpt(2));
	z=linspace(0, reg(3,3), ncpt(3));
	[X Y Z] = meshgrid(x, y, z);
	
    
	alpA = 1.0;
% 	patch( isosurface(X,Y,Z, ur0, isoA),...
% 		'facecolor', 'red', 'FaceAlpha', alpA, 'edgecolor','none');
% 	patch( isocaps(X,Y,Z, ur0, isoA, 'enclose'),...
% 		'facecolor', 'none', 'FaceAlpha', alpA, 'edgecolor','Interp');
 	colormap('winter');
    patch( isosurface(X,Y,Z, ur0, isoA),...
		'facecolor', [46,169,223]/255, 'FaceAlpha', alpA, 'edgecolor','none');
	patch( isocaps(X,Y,Z, ur0, isoA, 'enclose'),...
		'facecolor', 'none', 'FaceAlpha', alpA, 'edgecolor','flat');
 	colormap('jet');
    daspect([1, 1, 1]);
	camup([1, 0, 0]);
	campos([25, -55, 5]);
% view(-147, 45);
% view([-153 37]);
    view(0, -45);
    axis square;
	axis equal;
	axis tight;
	camlight;
	lighting phong;
	box off;
	axis off;
	set(gca, 'LineWidth', 2, 'FontSize', 30, 'FontName', 'Times New Roman');
	set(gcf, 'PaperUnits', 'inches', 'PaperPosition', Size);