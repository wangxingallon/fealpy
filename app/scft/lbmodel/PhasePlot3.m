function PhasePlot(DIM)
clc; close all

set(0, 'DefaultFigureVisible', 'off');
if exist('figure/') == 0
	mkdir('figure/');
end

indExtend = 0;

char srcfilename;
char saveName;

KN = [45, 45, 45];

fALft = 26;
fARgt = 26;	

fBLft = 48;
fBRgt = 48;

if DIM == 3

for fA = fALft:1:fARgt
	for fB = fBLft:1:fBRgt

		fC =100- fA - fB;

%	pause(2)

	char parameters;
	parameters = sprintf('[fA, fB, fC]: [%f,%f,%f]', fA, fB, fC);
	fprintf('parameters: %s\n', parameters);

	fnA = fA / 100;
	fnB = fB / 100;
	fnC = fC / 100;
    srcfilename = sprintf('phiA.[%.2f.%.2f.%.2f].[%.4f.%.4f.%.4f]-[128].dat',...
		KN(1), KN(2), KN(3), fnA, fnB, fnC);

%    if (fA < 10)
%        srcfilename = sprintf('phiA.[40.00.40.00.40.00].[0.0%d.0.%d.0.%d]-[32].dat', fA, fB, fC);
%    elseif (fB < 10)
%        srcfilename = sprintf('phiA.[40.00.40.00.40.00].[0.%d.0.0%d.0.%d]-[32].dat', fA, fB, fC);
%    elseif (fC < 10)
%        srcfilename = sprintf('phiA.[40.00.40.00.40.00].[0.%d.0.%d.0.0%d]-[32].dat', fA, fB, fC);
%    else
%        srcfilename = sprintf('phiA.[40.00.40.00.40.00].[0.%d.0.%d.0.%d]-[32].dat', fA, fB, fC);
%    end

DataA = load(strrep(srcfilename, 'A', 'A'));
DataB = load(strrep(srcfilename, 'A', 'B'));
DataC = load(strrep(srcfilename, 'A', 'C'));
[Line, Column] = size(DataA);

x = DataA(1,:);
y = DataA(2,:);
z = DataA(3,:);
xrange=max(x)-min(x);
yrange=max(y)-min(y);
zrange=max(z)-min(z);

[X Y Z] = meshgrid(x, y, z);

Line = Line - 3;
for i = [1:Line/Column]
    phiA(:,:,i) = DataA(4+(i-1)*Column:3+i*Column,:);
	phiB(:,:,i) = DataB(4+(i-1)*Column:3+i*Column,:);
	phiC(:,:,i) = DataC(4+(i-1)*Column:3+i*Column,:);
end

figure(1)
axis off
axis equal
set(gcf, 'color', 'white')

minphiA = min(min(min(phiA)));
maxphiA = max(max(max(phiA)));
isoA = minphiA+0.7*(maxphiA-minphiA);
%isoA = 0.45;
minphiB = min(min(min(phiB)));
maxphiB = max(max(max(phiB)));
isoB = minphiB+0.7*(maxphiB-minphiB);
%isoB = 0.45;
minphiC = min(min(min(phiC)));
maxphiC = max(max(max(phiC)));
isoC = minphiC+0.7*(maxphiC-minphiC);
%isoC = 0.45;

fprintf('Line=%d, Column=%d;  isoA=%.4f, isoB=%.4f, isoC=%.4f\n',...
	Line, Column, isoA, isoB, isoC);

xextend = X+xrange;
yextend = Y+yrange;
zextend = Z+zrange;

alpA = 0.9;
alpB = 0.2;
alpC = 0.9;

if indExtend == 0
	patch(isosurface(X,Y,Z, phiA, isoA), ...
	'facecolor','red', 'FaceAlpha', alpA, 'edgecolor','none');
	patch(   isocaps(X,Y,Z, phiA, isoA, 'enclose'), ...
	'facecolor','red','FaceAlpha', alpA, 'edgecolor','none');

	patch(isosurface(X,Y,Z, phiB, isoB), ... 
	'facecolor','green','FaceAlpha', alpB, 'edgecolor','none');
	patch(isocaps(X,Y,Z, phiB, isoB,'enclose'), ...
	'facecolor','green','FaceAlpha', alpB, 'edgecolor','none');

	patch(isosurface(X,Y,Z, phiC, isoC), ...
	'facecolor','blue','FaceAlpha', alpC, 'edgecolor','none');
	patch(   isocaps(X,Y,Z, phiC, isoC,'enclose'), ...
	'facecolor','blue','FaceAlpha', alpC, 'edgecolor','none');
else
	patch(isosurface(X,Y,Z, phiA, isoA), ...
	'facecolor','red', 'FaceAlpha', alpA, 'edgecolor','none');
	patch(   isocaps(X,Y,Z, phiA, isoA, 'enclose'), ...
	'facecolor','red','FaceAlpha', alpA, 'edgecolor','none');
	patch(isosurface(xextend,Y,Z, phiA, isoA),    'FaceAlpha', alpA,        'facecolor','red','edgecolor','none');
	patch(   isocaps(xextend,Y,Z, phiA, isoA ,'enclose'),'facecolor','red','FaceAlpha', alpA, 'edgecolor','none');
	patch(isosurface(X,yextend,Z, phiA, isoA),           'facecolor','red','FaceAlpha', alpA, 'edgecolor','none');
	patch(   isocaps(X,yextend,Z, phiA, isoA ,'enclose'),'facecolor','red','FaceAlpha', alpA, 'edgecolor','none');
	patch(isosurface(X,Y,zextend, phiA, isoA),           'facecolor','red','FaceAlpha', alpA, 'edgecolor','none');
	patch(   isocaps(X,Y,zextend, phiA, isoA ,'enclose'),'facecolor','red','FaceAlpha', alpA, 'edgecolor','none');
	patch(isosurface(xextend,yextend,Z, phiA, isoA),           'facecolor','red','FaceAlpha', alpA, 'edgecolor','none');
	patch(   isocaps(xextend,yextend,Z, phiA, isoA ,'enclose'),'facecolor','red','FaceAlpha', alpA, 'edgecolor','none');
	patch(isosurface(xextend,Y,zextend, phiA, isoA),           'facecolor','red','FaceAlpha', alpA, 'edgecolor','none');
	patch(   isocaps(xextend,Y,zextend, phiA, isoA ,'enclose'),'facecolor','red','FaceAlpha', alpA, 'edgecolor','none');
	patch(isosurface(X,yextend,zextend, phiA, isoA),           'facecolor','red','FaceAlpha', alpA, 'edgecolor','none');
	patch(   isocaps(X,yextend,zextend, phiA, isoA ,'enclose'),'facecolor','red','FaceAlpha', alpA, 'edgecolor','none');
	patch(isosurface(xextend,yextend,zextend, phiA, isoA),           'facecolor','red','FaceAlpha', alpA, 'edgecolor','none');
	patch(   isocaps(xextend,yextend,zextend, phiA,  isoA,'enclose'),'facecolor','red','FaceAlpha', alpA, 'edgecolor','none');


	patch(isosurface(X,Y,Z, phiB, isoB), ... 
	'facecolor','green','FaceAlpha', alpB, 'edgecolor','none');
	patch(   isocaps(X,Y,Z, phiB, isoB,'enclose'), ...
	'facecolor','green','FaceAlpha', alpB, 'edgecolor','none');
	patch(isosurface(xextend,Y,Z, phiB, isoB), 'facecolor','green','FaceAlpha', alpB, 'edgecolor','none');
	patch(   isocaps(xextend,Y,Z, phiB, isoB ,'enclose'),'facecolor','green','FaceAlpha', alpB, 'edgecolor','none');
	patch(isosurface(X,yextend,Z, phiB, isoB), 'facecolor','green','FaceAlpha', alpB, 'edgecolor','none');
	patch(   isocaps(X,yextend,Z, phiB, isoB ,'enclose'),'facecolor','green','FaceAlpha', alpB, 'edgecolor','none');
	patch(isosurface(X,Y,zextend, phiB, isoB), 'facecolor','green','FaceAlpha', alpB, 'edgecolor','none');
	patch(   isocaps(X,Y,zextend, phiB, isoB ,'enclose'),'facecolor','green','FaceAlpha', alpB, 'edgecolor','none');
	patch(isosurface(xextend,yextend,Z, phiB, isoB), 'facecolor','green','FaceAlpha', alpB, 'edgecolor','none');
	patch(   isocaps(xextend,yextend,Z, phiB, isoB ,'enclose'),'facecolor','green','FaceAlpha', alpB, 'edgecolor','none');
	patch(isosurface(xextend,Y,zextend, phiB, isoB), 'facecolor','green','FaceAlpha', alpB, 'edgecolor','none');
	patch(   isocaps(xextend,Y,zextend, phiB, isoB ,'enclose'),'facecolor','green','FaceAlpha', alpB, 'edgecolor','none');
	patch(isosurface(X,yextend,zextend, phiB, isoB), 'facecolor','green','FaceAlpha', alpB, 'edgecolor','none');
	patch(   isocaps(X,yextend,zextend, phiB, isoB ,'enclose'),'facecolor','green','FaceAlpha', alpB, 'edgecolor','none');
	patch(isosurface(xextend,yextend,zextend, phiB, isoB), 'facecolor','green','FaceAlpha', alpB, 'edgecolor','none');
	patch(   isocaps(xextend,yextend,zextend, phiB, isoB,'enclose'),'facecolor','green','FaceAlpha', alpB, 'edgecolor','none');

	patch(isosurface(X,Y,Z, phiC, isoC), ...
	'facecolor','blue','FaceAlpha', alpC, 'edgecolor','none');
	patch(   isocaps(X,Y,Z, phiC, isoC,'enclose'), ...
	'facecolor','blue','FaceAlpha', alpC, 'edgecolor','none');
	patch(isosurface(xextend,Y,Z, phiC, isoC), 'facecolor','blue','FaceAlpha', alpC, 'edgecolor','none');
	patch(   isocaps(xextend,Y,Z, phiC, isoC ,'enclose'),'facecolor','blue','FaceAlpha', alpC, 'edgecolor','none');
	patch(isosurface(X,yextend,Z, phiC, isoC), 'facecolor','blue','FaceAlpha', alpC, 'edgecolor','none');
	patch(   isocaps(X,yextend,Z, phiC, isoC ,'enclose'),'facecolor','blue','FaceAlpha', alpC, 'edgecolor','none');
	patch(isosurface(X,Y,zextend, phiC, isoC), 'facecolor','blue','FaceAlpha', alpC, 'edgecolor','none');
	patch(   isocaps(X,Y,zextend, phiC, isoC ,'enclose'),'facecolor','blue','FaceAlpha', alpC, 'edgecolor','none');
	patch(isosurface(xextend,yextend,Z, phiC, isoC), 'facecolor','blue','FaceAlpha', alpC, 'edgecolor','none');
	patch(   isocaps(xextend,yextend,Z, phiC, isoC ,'enclose'),'facecolor','blue','FaceAlpha', alpC, 'edgecolor','none');
	patch(isosurface(xextend,Y,zextend, phiC, isoC), 'facecolor','blue','FaceAlpha', alpC, 'edgecolor','none');
	patch(   isocaps(xextend,Y,zextend, phiC, isoC ,'enclose'),'facecolor','blue','FaceAlpha', alpC, 'edgecolor','none');
	patch(isosurface(X,yextend,zextend, phiC, isoC), 'facecolor','blue','FaceAlpha', alpC, 'edgecolor','none');
	patch(   isocaps(X,yextend,zextend, phiC, isoC ,'enclose'),'facecolor','blue','FaceAlpha', alpC, 'edgecolor','none');
	patch(isosurface(xextend,yextend,zextend, phiC, isoC), 'facecolor','blue','FaceAlpha', alpC, 'edgecolor','none');
	patch(   isocaps(xextend,yextend,zextend, phiC, isoC,'enclose'),'facecolor','blue','FaceAlpha', alpC, 'edgecolor','none');

end

view(-30, 28)
camlight
lighting gouraud

    saveName = sprintf('phi.[%.2f.%.2f.%.2f].[%.4f.%.4f.%.4f]-[32]',...
		KN(1), KN(2), KN(3), fnA, fnB, fnC);
%    if (fA < 10)
%	    saveName = sprintf('HDG.phi.[40.00.40.00.40.00].[0.0%d.0.%d.0.%d]-[32]', fA, fB, fC);
%    elseif (fB < 10)
%	    saveName = sprintf('HDG.phi.[40.00.40.00.40.00].[0.%d.0.0%d.0.%d]-[32]', fA, fB, fC);
%    elseif (fC < 10)
%	    saveName = sprintf('HDG.phi.[40.00.40.00.40.00].[0.%d.0.%d.0.0%d]-[32]', fA, fB, fC);
%    else
%	    saveName = sprintf('HDG.phi.[40.00.40.00.40.00].[0.%d.0.%d.0.%d]-[32]', fA, fB, fC);
%    end
	title(saveName);
	set(gcf, 'unit', 'normalized', 'position', [0.1, 0.1, 0.8, 0.8])
	f = getframe(gcf);
	imwrite(f.cdata,  ['figure/', saveName, '.png']);
%	pause(2)

close all
fprintf('finish!\n\n');

end

end 
end

%energy = load('hamilton.dat');
%plot(energy(:,4));
%set(gcf, 'color', 'white')
%saveName = sprintf('HDG.hamilton.[40.00.40.00.40.00].fA-[0.%d-0.%d].fB-[0.%d-0.%d]-[32]', fALft, fARgt, fBLft, fBRgt);
%title(saveName);
%axis on
%set(gcf, 'unit', 'normalized', 'position', [0.1, 0.1, 0.8, 0.8])
%f = getframe(gcf);
%imwrite(f.cdata,  [saveName, '.png']);
%pause(1)

%res = load('res.dat');
%figure(2)
%plot(res(:,1));
%title('Energy')
%
%figure(3)
%semilogy(res(:,2));
%title('Energy Difference')
%
%figure(4)
%semilogy(res(:,3), 'r'); 
%hold on; 
%semilogy(res(:,4), 'g');
%semilogy(res(:,5), 'b');
%title('Residual Gradient');

