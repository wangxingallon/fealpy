%%%%%%%%%%%  Computing ordered  on Ohta-Kawasaki model by CH equation
%%%%%%%%%%%  Projection method with P=I is used in this code
%%%%%%%%%%%  The semi-implicit scheme is the primary algorithm to minimize the problem
%%%%%%%%%%%  At the same time, the Nesterov method is used to accelerate the optimization algorithm
%%%%%%%%%%%  The time step size is estimated by mean eigevalue of Hessian matrix
%%%%%%%%%%%  data:  2020-03-03

function [hamilton, symmflag] = okmodel(PATTERN, dim, uCplx, nr, k2_nc, k2_nr, tau, gamma, rcpBox, calculate, fname)
	
    format long;
	close all;

	ncpt = size(uCplx);

	uCplx_cs = uCplx;
	if dim(1) == 1 uCplx_rf = zeros(1, nr);
	else uCplx_rf = zeros(nr);
	end

	type = 'cube';
%    iterMethod = 'APG';
	iterMethod = 'HSSI';
%    iterMethod = 'SIS';
	scaleQ = 1;

	DimCpt = dim(1);
	DimPhy = dim(2);

	%%%%%% model systems %%%%%%%
	sysPmts(1) = 1.0;   %%%  scale 
	sysPmts(2) = 1.0;   %%%  interaction term
	sysPmts(3) = tau;   %%%  2nd term
	sysPmts(4) = gamma;   %%%  3rd term
	sysPmts(5) = 1;   %%%  4th term

	if strcmp(iterMethod, 'SIS')
		tstep = 0.1;
	else
		tstep = 0.1;
	end
	tmin = 1.0e-1;
	tmax = 2.;

	t0 = tstep;
	diffham = inf;
	hamOld = inf;
	hamilton = inf;
	iterator = 0;
	err = 1.0;
	itMax = 5000;
	tol_cs = 5.0e-4;
	tol_rf = 1.0e-6;

	cmpPmts(1) = tol_cs;
	cmpPmts(2) = itMax;
	cmpPmts(3) = tstep;
	cmpPmts(4) = tmin;
	cmpPmts(5) = tmax;
	
	EPS = 1.0;
	cout = 0;
	cout_max = 20;

	tic
	%%%  fixed box
	if strcmp(iterMethod, 'SIS')
		if strcmp(calculate, 'check')
			uCplx_rf = uCplx;
		else
			[ham0, uCplx_cs] = oksis(k2_nc, sysPmts, dim, cmpPmts, rcpBox, uCplx, fname);
			uCplx_rf = mymap(uCplx_cs, uCplx_rf, 'cs2rf');
		end
		cmpPmts(1) = tol_rf;
		[ham0, uCplx_rf] = oksis(k2_nr, sysPmts, dim, cmpPmts, rcpBox, uCplx_rf, fname);

	elseif strcmp(iterMethod, 'HSSI')
		if strcmp(calculate, 'check')
			uCplx_rf = uCplx;
		else
			[ham0, uCplx_cs] = okhssi(k2_nc, sysPmts, dim, cmpPmts, rcpBox, uCplx, fname);
			uCplx_rf = mymap(uCplx_cs, uCplx_rf, 'cs2rf');
		end
		cmpPmts(1) = tol_rf;
		[ham0, uCplx_rf] = okhssi(k2_nr, sysPmts, dim, cmpPmts, rcpBox, uCplx_rf, fname);

	elseif strcmp(iterMethod, 'APG')
		if strcmp(calculate, 'check')
			uCplx_rf = uCplx;
		else
			[ham0, uCplx_cs] = okapg(k2_nc, sysPmts, dim, cmpPmts, rcpBox, uCplx, fname);
			uCplx_rf = mymap(uCplx_cs, uCplx_rf, 'cs2rf');
		end
		cmpPmts(1) = tol_rf;
		[ham0, uCplx_rf] = okapg(k2_nr, sysPmts, dim, cmpPmts, rcpBox, uCplx_rf, fname);
	end

	while (EPS > 1.0e-6 && cout < cout_max)
		cout = cout+1;

		[rcpBox,k2] = okbox(sysPmts, dim, uCplx_rf, rcpBox, 1.0e-5, type);
		fprintf('\n\n');

		if strcmp(iterMethod, 'SIS')
			[ham1, uCplx_rf] = oksis(k2, sysPmts, dim, cmpPmts, rcpBox, uCplx_rf, fname);
		elseif strcmp(iterMethod, 'HSSI')
			[ham1, uCplx_rf] = okhssi(k2, sysPmts, dim, cmpPmts, rcpBox, uCplx_rf, fname);
		elseif strcmp(iterMethod, 'APG')
			[ham1, uCplx_rf] = okapg(k2, sysPmts, dim, cmpPmts, rcpBox, uCplx_rf, fname);
		end
		EPS = abs(ham1 - ham0);
		ham0 = ham1;
		fprintf('cout %d : hamilton = %.15e\t EPS = %.15e\n\n\n', cout, ham0, EPS);

	end
	toc

	if strcmp(PATTERN, 'sigma')
		symmflag = 2;
	else
		uc0 = obtainIniVals(PATTERN, nr, dim);
		symmflag = detsymmetry(uc0, uCplx_rf);
	end

	hamilton = ham0

	dirBox = getDualBox(rcpBox, scaleQ)
	plotPhase(PATTERN, uCplx_rf, dirBox, dim, rcpBox, fname);
	ncpt = nr;		uCplx = uCplx_rf;
    save([fname, '.mat'], 'uCplx', 'ncpt', 'rcpBox', 'dim', 'hamilton', 'symmflag');
end


