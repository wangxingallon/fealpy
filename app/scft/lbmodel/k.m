% ncpt =[128,128,64]
ncpt =[10,10,6];
dirBox = zeros(3,3);
dirBox(1,1)=17.77;
dirBox(2,2)=17.77;
dirBox(3,3)=9.39;

rcpBox = 2*pi*inv(dirBox);

for j1=1:1:ncpt(1)
		if (j1>ncpt(1)/2+1) k1=j1-ncpt(1);
		else k1 = j1;
		end
		for j2=1:1:ncpt(2)
			if (j2>ncpt(2)/2+1) k2=j2-ncpt(2);
			else k2 = j2;
			end
			for j3=1:1:ncpt(3)
				if (j3>ncpt(3)/2+1) k3=j3-ncpt(3);
				else k3 = j3;
				end
				kk = [k1, k2, k3];
 				kk = kk-1;
 				ksquare(j1,j2,j3) = kk*(rcpBox')*rcpBox*(kk');
			end
		end
	end

