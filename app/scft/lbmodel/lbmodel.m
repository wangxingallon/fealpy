%function ucplx = lpmodel()

clear; clc; close all;
%set(0, 'DefaultFigureVisible', 'on');
set(0, 'DefaultFigureVisible', 'off');

%% PATTERN = 'hex', 'bcc', 'fcc';
% PATTERN = 'hex';
PATTERN = 'bcc';
%PATTERN = 'fcc';

doc = sprintf('results/%s/', PATTERN);
if exist(doc) == 0
	mkdir(doc);
end


%%%%%%%%%%%% set model parameters %%%%%%%%%%%%%
Sys.B0 = 150;
Sys.xi = 0.015;
Sys.t = 1.0;
Sys.v = 1.0;
ncptN = 16;
dt = 0.1;

Sys.q0 = 1.0;
Sys.xi = -Sys.xi;
scaleQ = 1;

%% dimension and box;
if strcmp(PATTERN, 'hex')
	dim = [2,2];
	rcpBox = [
					1, 0;
			cos(pi/3), sin(pi/3)
			];
elseif strcmp(PATTERN, 'bcc')
	dim = [3,3];
	L = sqrt(2);
	rcpBox = zeros(dim(1));
	for j=1:1:dim(1) rcpBox(j,j)=scaleQ/L; end
elseif strcmp(PATTERN, 'fcc')
	dim = [3,3];
	L = sqrt(3);
	rcpBox = zeros(dim(1));
	for j=1:1:dim(1) rcpBox(j,j)=scaleQ/L; end
else
	fprintf('WARING: %s out range', PATTERN);
end
DimCpt = dim(1);
DimPhy = dim(2);
rcpBox = rcpBox';

ncpt = ones(1,DimCpt);
ncpt(:) = ncptN;
dof = prod(ncpt);

%%% allocation memory
ucplx = zeros(ncpt);
ureal = zeros(ncpt);
u2real = zeros(ncpt);
u3real = zeros(ncpt);
u4real = zeros(ncpt);
ksquare = zeros(ncpt);
kterm = zeros(ncpt);

%%% obtain initial values
iniVal = markGlobalPattern(PATTERN);
kindex = iniVal(:, 1:1:end-1);
[nr, nc] = size(kindex);
for i=1:1:nr
  for j=1:1:nc
	  if (kindex(i,j)<0) 
		  kindex(i,j)=kindex(i,j)+ncpt(j);
	  end
  end
end
kindex = kindex+1;
if DimCpt==2
	for k=1:1:nr
	   ucplx(kindex(k,1),kindex(k,2)) = iniVal(k,end);
	end
elseif DimCpt==3
	for k=1:1:nr
	   ucplx(kindex(k,1),kindex(k,2),kindex(k,3)) = iniVal(k,end);
	end
else
	error('Warning: dimension is wrong');
end
u0 = ucplx;
ureal = real(fftn(ucplx));


%% plot
dirBox = getDualBox(rcpBox, scaleQ);
fname = sprintf('%sinit', doc);
plotPhase(u0, dirBox, dim, rcpBox, fname);
%return;


%%%%  get Laplace operater in K-space
if DimCpt == 2
	for j1=1:1:ncpt(1)
		if (j1>ncpt(1)/2+1) k1=j1-ncpt(1);
		else k1 = j1;
		end
		for j2=1:1:ncpt(2)
			if (j2>ncpt(2)/2+1) k2=j2-ncpt(2);
			else k2 = j2;
			end
			kk = [k1, k2];
			kk = kk-1;
			ksquare(j1,j2) = kk*(rcpBox')*rcpBox*(kk');
		end
	end
elseif DimCpt == 3
	for j1=1:1:ncpt(1)
		if (j1>ncpt(1)/2+1) k1=j1-ncpt(1);
		else k1 = j1;
		end
		for j2=1:1:ncpt(2)
			if (j2>ncpt(2)/2+1) k2=j2-ncpt(2);
			else k2 = j2;
			end
			for j3=1:1:ncpt(3)
				if (j3>ncpt(3)/2+1) k3=j3-ncpt(3);
				else k3 = j3;
				end
				kk = [k1, k2, k3];
				kk = kk-1;
				ksquare(j1,j2,j3) = kk*(rcpBox')*rcpBox*(kk');
			end
		end
	end
end
ksquare
%%%%  get Laplace operater in K-space
kterm = Sys.q0^2 - ksquare;

%%% &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
ek = kterm.*ucplx;
ek = real(fftn(ek)).^2;
dtmp = ifftn(ek);
init_ediff = real(dtmp(1));

u2real = ureal.*ureal;
u3real = u2real.*ureal;
u4real = u3real.*ureal;

eb = -0.5*Sys.xi*u2real - (1/3)*Sys.t*u3real + 0.25*Sys.v*u4real;
btmp = ifftn(eb);
init_ebulk = real(btmp(1));
%%% &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

tol = 1.0e-5;
res = Inf;
iter = 0;
flow = -ksquare;

%%% update order parameter u 
t1loop = clock;

while (res > tol)

	iter = iter + 1;

	u2real = ureal.*ureal;
	u3real = u2real.*ureal;
	u4real = u3real.*ureal;
	
	%%%  calculate energy density %%%%
	ek = kterm.*ucplx;
	ek = real(fftn(ek)).^2;
	dtmp = ifftn(ek);
	ediff = real(dtmp(1));

	eb = -0.5*Sys.xi*u2real - (1/3)*Sys.t*u3real + 0.25*Sys.v*u4real;
	btmp = ifftn(eb);
	ebulk = real(btmp(1));

	energy = 0.5*Sys.B0*ediff + ebulk;


	u2 = ifftn(u2real);
	eb2 = ifftn(-0.5*Sys.xi*u2real);
	eb2 = real(eb2(1));

	u3 = ifftn(u3real);
	eb3 = ifftn(-(1/3)*Sys.t*u3real);
	eb3 = real(eb3(1));
	
	u4 = ifftn(u4real);
	eb4 = ifftn( 0.25*Sys.v*u4real);
	eb4 = real(eb4(1));

%    spli = [0.5*Sys.B0*ediff, eb2, eb3, eb4]
%   pause()


	%%%  calculate energy density %%%%
	nonlinear = -Sys.xi*ureal - Sys.t*u2real + u3real;
	kucplx = (kterm.^2).*ucplx;
	
	%% error;
    gradient = Sys.B0* real( fftn(kucplx) ) + nonlinear;
	gradient = flow .* ifftn(gradient);
    %%% mean of u equals 0
    gradient(1) = 0;
	gradient = real(fftn(gradient));
	res = max(abs(gradient(:)));


	%% iterator;
	ucplx = ( ucplx + dt*flow.*ifftn(nonlinear) ) ./...
		( 1 - dt*Sys.B0*flow.*(kterm.^2) );
	ucplx(1) = 0;
	ureal = real(fftn(ucplx));

	if mod(iter, 100) == 0
		fname = sprintf('%s%d', doc, iter);
		plotPhase(ucplx, dirBox, dim, rcpBox, fname);
	end

	fprintf('iter %d : res = %.15e, energy = %.15e\n', iter, res, energy);
end
 
t2loop = clock;
time = etime(t2loop,t1loop); 
fprintf('Time cost of while-loop is " %.6f " seconds\n\n\n', time);

%% plot
fname = sprintf('%sconvergence', doc);
plotPhase(ucplx, dirBox, dim, rcpBox, fname);


%end
