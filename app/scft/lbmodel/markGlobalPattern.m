function kindex = markGlobalPattern(PATTERN)

if strcmp(PATTERN, 'hex')
	kindex=[
			1     0     0.3 
		   -1     0     0.3 
			0     1     0.3 
		    0    -1     0.3 
		    1    -1     0.3 
		   -1     1     0.3 
	];
elseif strcmp(PATTERN, 'bcc')
    kindex=[
        1	 1	 0	0.3;
       -1	 1	 0	0.3;
       -1	-1	 0	0.3;
        1	-1	 0	0.3;
        0	 1	 1	0.3;
        0	-1	 1	0.3;
        0	-1	-1	0.3;
        0	 1	-1	0.3;
        1	 0	 1	0.3;
       -1	 0	 1	0.3;
       -1	 0	-1	0.3;
        1	 0	-1	0.3
        ];
elseif strcmp(PATTERN, 'fcc')
    kindex=[
        1	 1	 1	0.3;
        1	-1	 1	0.3;
       -1	 1	 1	0.3;
       -1	-1	 1	0.3;
        ];
end

end
