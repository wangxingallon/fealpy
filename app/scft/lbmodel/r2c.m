N =16;
load('12fold16-2q.mat');
urA = ucplx;
urB = 1-urA;
ucA =  zeros(N,N,N,N,N);
ucB =  zeros(N,N,N,N,N);
ucA = ifftn(urA);
ucB = ifftn(urB);
phiA = ucA(:,:,:,:,1:N/2+1);
phiB = ucB(:,:,:,:,1:N/2+1);

X=ones(N,N,N,N,N/2+1);
Y=ones(N,N,N,N,N/2+1);

% get X %
X1=[0:N/2 -N/2+1:-1]';
X2=ones(1,N);

Xk=kron(X1,X2);
for k=1:N/2+1
    X(:,:,:,:,k)=Xk;
end

% get Y% 
Y1=ones(N,1);
Y2=[0:N/2 -N/2+1:-1];

Yk=kron(Y1,Y2);
for k=1:N/2+1
    Y(:,:,:,:,k)=Yk;
end

% get Z%
Z1=ones(N,1);
Z2=ones(1,N);

Zk=kron(Z1,Z2);
for k=1:N/2+1
    Z(:,:,:,:,k)=(k-1)*Zk;
end

% get file-phiA%
[phiA_value,phiA_index]=sort(phiA(:),'descend');

Xindex_A=X(phiA_index);
Yindex_A=Y(phiA_index);
Zindex_A=Z(phiA_index);
A_value_R=real(phiA_value);
A_value_C=imag(phiA_value);

fid=fopen('phiA12.txt','wt');
fprintf(fid,'%g\n',N*N*N*N*(N/2+1));

for k=1:(N*N*N*N*(N/2+1))
    fprintf(fid,'%g\t',Xindex_A(k));
     fprintf(fid,'%g\t',Yindex_A(k)); 
     fprintf(fid,'%g\t',Zindex_A(k));
      fprintf(fid,'%e\t',A_value_R(k));
       fprintf(fid,'%e\t',A_value_C(k));
       
        fprintf(fid,'\n');
end
fclose(fid);
% get file-phiB%
[phiB_value,phiB_index]=sort(phiB(:),'descend');

Xindex_B=X(phiB_index);
Yindex_B=Y(phiB_index);
Zindex_B=Z(phiB_index);
B_value_R=real(phiB_value);
B_value_C=imag(phiB_value);

fid=fopen('phiB12.txt','wt');
fprintf(fid,'%g\n',N*N*N*N*(N/2+1));

for k=1:N*N*N*N*(N/2+1)
    fprintf(fid,'%g\t',Xindex_B(k));
     fprintf(fid,'%g\t',Yindex_B(k));
      fprintf(fid,'%g\t',Zindex_B(k));
      fprintf(fid,'%e\t',B_value_R(k));
       fprintf(fid,'%e\t',B_value_C(k));
       
        fprintf(fid,'\n');
end
fclose(fid);
