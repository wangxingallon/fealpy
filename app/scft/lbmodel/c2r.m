load('Sigma.mat');
ucs = wA;
ucs = reshape(ucs,128,128,64);
ucs= ifftn(ucs);
urf = zeros(128,128,128);
urf = mymap(ucs, urf, 'cs2rf');
wA =real(fftn(urf));
ucs = wB;
ucs = reshape(ucs,128,128,64);
ucs= ifftn(ucs);
urf = zeros(128,128,128);
urf = mymap(ucs, urf, 'cs2rf');
wB =real(fftn(urf));
save('sigma.mat','wA','wB')
