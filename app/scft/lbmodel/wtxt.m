load('12fold16-last.mat');
fid=fopen('phiA12.txt','wt');
fprintf(fid,'%g\n',N*N*N*N*(N/2+1));

for k=1:24
    fprintf(fid,'%g\t',kindex(k,1)-N);
     fprintf(fid,'%g\t', kindex(k,2)-N); 
     fprintf(fid,'%g\t', kindex(k,3)-N);
     if k<13
         fprintf(fid,'%e\t', 0.0981);
         fprintf(fid,'%e\t',0);
     else
        fprintf(fid,'%e\t', 0.0765);
         fprintf(fid,'%e\t',0);
     end
   fprintf(fid,'\n');
end
