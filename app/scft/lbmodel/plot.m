%  load('../data398.mat')
% lopad('../i.phiA.14.00.0.30.dat')
 q = reshape(q,32,32,32);
 ucplx = fftn(q);
dim = [3,3];
dirBox = box;
% dirBox = zeros(3,3);
% dirBox(1,1)=0.71;
% dirBox(2,2)=0.71;
% dirBox(3,3)=0.71;
rcpBox = 2*pi*inv(dirBox);
fname = strcat('bcc');
plotPhase(ucplx, dirBox, dim, rcpBox, fname);
