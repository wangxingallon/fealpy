
%%%% Expand [128, 128, 64] to obtain more initial values;
clear; clc; close all;
set(0, 'DefaultFigureVisible', 'on');

%%%%% Initial Values;
tmp = load('Sigma128x128x64.txt');
uR0 = reshape(tmp(:,4), [128, 128, 64]);
uC0 = ifftn(uR0);
uC0 = fftshift(uC0);


%ExN = 32;	%% [256, 256, 128];
%%%% 2, 4, 6, 8, 10, 12, 14, 16, 18, 20;
ExNTot = 2:2:30;
for i = 1:1:length(ExNTot)
	ExpandInitialValues( uC0, ExNTot(i) );
end


function ExpandInitialValues( uC0, ExN )

	ncpt = [128+4*ExN, 128+4*ExN, 64+2*ExN];
	dof = prod(ncpt);
	uCplx = zeros(ncpt);

	%for j1 = 1:1:128
	%	for j2 = 1:1:128
	%		for j3 = 1:1:64
	%			uCplx(j1+2*ExN, j2+2*ExN, j3+ExN) = uC0(j1, j2, j3);
	%		end
	%	end
	%end
	uCplx([1:1:128]+2*ExN, [1:1:128]+2*ExN, [1:1:64]+ExN) = uC0;
	uCplx = fftshift(uCplx);
	uReal = real(fftn( uCplx ));

	if ( ncpt == [256, 256, 128] )
		fprintf('Notation: [256, 256, 128]\n');	
	elseif ( ncpt == [128, 128, 64] )
		fprintf('Notation: [128, 128, 64]\n');	
	else
		DataPath = sprintf('Sigma%dx%dx%d.txt', ncpt(1), ncpt(2), ncpt(3));
		fid = fopen(DataPath, 'w');
		for i = 1:1:dof
			fprintf(fid, '%.6g\n', uReal(i));
		end
		fclose(fid);
	end


	%%% Check;
	if ( ncpt == [256, 256, 128] )
		tmp = load('Sigma256x256x128.txt');
		uR0 = reshape(tmp(:,4), [256, 256, 128]);
		uC0 = ifftn(uR0);
		Err = max(abs( uC0(:) - uCplx(:) ));
		fprintf('Error: %.12e\n', Err);
	end

end

