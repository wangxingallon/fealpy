function kspace = projPlane(uCplx, rcpBox, ncpt, dim)
   
	dof = prod(ncpt);
	index = 0;

	DimPhy = dim(1);
	DimCpt = dim(2);
	projmat = rcpBox;

	kspace = zeros(dof, DimPhy+1);

	if DimCpt == 2 && DimPhy == 2
	   for j1=1:1:ncpt(1)
		   if (j1>ncpt(1)/2) k1=j1-ncpt(1);
		   else k1 = j1;
		   end
		   for j2=1:1:ncpt(2)
			   if (j2>ncpt(2)/2) k2=j2-ncpt(2);
			   else k2 = j2;
			   end
			   index = index+1;
			   kk = [k1, k2];
			   kk = kk-1;
			   klow = projmat*(kk');
			   kspace(index,1)=klow(1);
			   kspace(index,2)=klow(2);
			   kspace(index,3)=uCplx(j1,j2);
		   end
	   end
	end

end
