%% obtain the initial reciprocal value and box;
function [uinit, rcpBox, dim] = IniCplxBox(PATTERN, nc, scaleQ)

	%%%%   dim(1) = DimCpt; dim(2) = DimPhy;

	if strcmp(PATTERN, 'lam')
		dim = [2,2];
		L = 1;
		rcpBox = zeros(dim(1));
		for j=1:1:dim(1) rcpBox(j,j)=scaleQ/L; end
		uinit = obtainIniVals(PATTERN, nc, dim);
	elseif strcmp(PATTERN, 'gyroid')
		dim = [3,3];
		L = sqrt(5);
		rcpBox = zeros(dim(1));
		for j=1:1:dim(1) rcpBox(j,j)=scaleQ/L; end
		uinit = obtainIniVals(PATTERN, nc, dim);
	elseif strcmp(PATTERN, 'hex')
		dim = [2,2];
		rcpBox = [
						1, 0;
				cos(pi/3), sin(pi/3)
				];
		uinit = obtainIniVals(PATTERN, nc, dim);
	elseif strcmp(PATTERN, 'hcp')
		dim = [3,3];
		L = sqrt(2);
		rcpBox = zeros(dim(1));
		for j=1:1:dim(1) rcpBox(j,j)=scaleQ/L; end
		uinit = obtainIniVals(PATTERN, nc, dim);
	elseif strcmp(PATTERN, 'bcc')
		dim = [3,3];
		L = sqrt(2);
		rcpBox = zeros(dim(1));
		for j=1:1:dim(1) rcpBox(j,j)=scaleQ/L; end
		uinit = obtainIniVals(PATTERN, nc, dim);
	elseif strcmp(PATTERN, 'fcc')
		dim = [3,3];
		L = sqrt(3);
		rcpBox = zeros(dim(1));
		for j=1:1:dim(1) rcpBox(j,j)=scaleQ/L; end
		uinit = obtainIniVals(PATTERN, nc, dim);
	elseif strcmp(PATTERN, 'A15')
		dim = [3,3];
		L = sqrt(5);
		rcpBox = zeros(dim(1));
		for j=1:1:dim(1) rcpBox(j,j)=scaleQ/L; end
		uinit = obtainIniVals(PATTERN, nc, dim);
	elseif strcmp(PATTERN, 'fddd')
		dim = [3,3];
		dirBox = [  pi, 0, 0; 
					0, 2*pi, 0; 
					0, 0, 2*sqrt(3)*pi
				];
		rcpBox = getDualBox(dirBox, scaleQ);
		uinit = obtainIniVals(PATTERN, nc, dim);
	elseif strcmp(PATTERN, 'fdddCube')
		dim = [3,3];
		L = 1/7.92;
		rcpBox = zeros(dim(1));
		for j=1:1:dim(1) rcpBox(j,j)=scaleQ/L; end
		uinit = obtainIniVals(PATTERN, nc, dim);
	%%   load from file
	%    nc = [24,24,24];
	%    tmp = load('./initvals/FdddCube/fdddcubeA');
	%    ur0 = reshape(tmp(4:end,:), nc);
	%    uinit = myifftn(ur0);
	%
	%    sflag = detsymmetry(uinit, uinit);
	%
	%    dirBox = [tmp(1,end), 0, 0;
	%                0,  tmp(2,end), 0;
	%                0,  0, tmp(3,end)];
	%    rcpBox = getDualBox(dirBox, scaleQ);
	%    clear tmp;
	elseif strcmp(PATTERN, 'sigma')
		dim = [3,3];
		if nc(end) == 64
			tmp = load('./initvals/SigmaPhase/Sigma128x128x64.txt');
		elseif nc(end) == 128
			tmp = load('./initvals/SigmaPhase/Sigma256x256x128.txt');
		end
		ur0 = reshape(tmp(:,4), nc);
		uinit = myifftn(ur0);
		%%
		dirBox = [tmp(end, 1), 0, 0; 
				  0, tmp(end, 2), 0;
				  0, 0, tmp(end, 3)];
		rcpBox = getDualBox(dirBox, scaleQ);
		clear tmp;
	end
end
