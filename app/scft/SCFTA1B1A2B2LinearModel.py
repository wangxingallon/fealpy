import numpy as np
import matplotlib.pyplot as plt
import math

from fealpy.functionspace import FourierSpace
from fealpy.timeintegratoralg.timeline import UniformTimeLine

from ParabolicFourierSolver import ParabolicFourierSolver
from numpy.linalg import inv

init_value = {
        "bcc":( 
        np.array([
            [ 1,    1,     0],
            [-1,    1,     0],
            [-1,   -1,     0],
            [ 1,   -1,     0],
            [ 0,    1,     1],
            [ 0,   -1,     1],
            [ 0,   -1,    -1],
            [ 0,    1,    -1],
            [ 1,    0,     1],
            [-1,    0,     1],
            [-1,    0,    -1],
            [ 1,    0,    -1]], dtype=np.int),
            np.array([0.3], dtype=np.float)
            ),
        "fcc":( 
        np.array([
            [ 1,    1,     1],
            [ 1,   -1,     1],
            [-1,    1,     1],
            [-1,   -1,     1]], dtype=np.int),
            np.array([0.3], dtype=np.float)
            ),
        "hex":( 
        np.array([
            [ 1,     0],
            [-1,     0],
            [ 0,     1],
            [ 0,    -1],
            [ 1,    -1],
            [-1,     1]], dtype=np.int),
            np.array([0.3], dtype=np.float)
            ),
        "bccw":( 
        np.array([
            [ 1,    1,     0],
            [-1,    1,     0],
            [-1,   -1,     0],
            [ 1,   -1,     0],
            [ 0,    1,     1],
            [ 0,   -1,     1],
            [ 0,   -1,    -1],
            [ 0,    1,    -1],
            [ 1,    0,     1],
            [-1,    0,     1],
            [-1,    0,    -1],
            [ 1,    0,    -1]], dtype=np.int),
            np.array([1.0], dtype=np.float)
            ),
        "fcc":( 
        np.array([
            [ 1,    1,     1],
            [ 1,   -1,     1],
            [-1,    1,     1],
            [-1,   -1,     1]], dtype=np.int),
            np.array([1], dtype=np.float)
            ),
        "LAM":(
	 np.array([[3, 0], [-3, 0]], dtype=np.int),
         np.array([0.058+0.2j, 0.058+0.2j], dtype=np.complex)
         )
        }

def model_options(
        nspecies = 2,
        nblend = 2,
        nblock1 = 2,
        nblock2 = 2,
        ndeg = 100,
        fA1 = 0.25,
        fA2 = 0.25,
        chiAB = 0.25,
        box = np.diag(2*[2*np.pi]),
        NS = 256,
        maxdt = 0.01,
        bA = 1,
        bB = 1,
        gamma = 1,
        phi2 = 1.5,
        Maxit = 5000,
        tol = 1e-7):
        # the parameter for scft model
        options = {
                'nspecies': nspecies,
                'nblend': nblend,
                'nblock1': nblock1,
                'nblock2': nblock2,
                'ndeg': ndeg,
                'fA1': fA1,
                'fA2': fA2,
                'chiAB': chiAB,
                'box': box,
                'dim': len(box),
                'NS' : NS,
                'maxdt': maxdt,
                'bA': bA,
                'bB': bB,
                'gamma': gamma,
                'phi2': phi2,
                'Maxit':Maxit,
                'tol':tol
                }
        return options


class SCFTA1B1A2B2LinearModel():
    def __init__(self, options=None):
        if options == None:
            options = pscftmodel_options()
        self.options = options
        dim = options['dim']
        box = options['box']

        self.space = FourierSpace(box,  options['NS'])

        fA1 = options['fA1']
        fB1 = 1-fA1
        fA2 = options['fA2']
        fB2  = 1-fA2
        maxdt = options['maxdt']
        self.gamma = options['gamma']

        self.phi2 = options['phi2']
        self.phi1 = 1 - self.phi2

        self.timelines1 = []
        self.timelines1.append(UniformTimeLine(0, fA1, int(np.ceil(fA1/maxdt))))
        self.timelines1.append(UniformTimeLine(0, fB1,  int(np.ceil(fB1/maxdt))))
        self.timelines2 = []
        self.timelines2.append(UniformTimeLine(0, self.gamma*fA2,int(np.ceil(self.gamma*fA2/maxdt))))
        self.timelines2.append(UniformTimeLine(0, self.gamma*fB2,int(np.ceil(self.gamma*fB2/maxdt))))


        self.pdesolvers1 = []
        for i in range(options['nblock1']):
            self.pdesolvers1.append(
                    ParabolicFourierSolver(self.space, self.timelines1[i])
                    )
        self.pdesolvers2 = []
        for i in range(options['nblock2']):
            self.pdesolvers2.append(
                    ParabolicFourierSolver(self.space, self.timelines2[i])
                    )
   
        # total number of time levels of A1B1
        self.TNL1 = 0         
        for i in range(options['nblock1']):
            self.TNL1 += self.timelines1[i].number_of_time_levels()
        self.TNL1 -= options['nblock1'] - 1
        
        # total number of time levels of A2B2
        self.TNL2 = 0
        for i in range(options['nblock2']):
            self.TNL2 += self.timelines2[i].number_of_time_levels()
        self.TNL2 -= options['nblock2'] - 1

        ## propagators of A1B1
        self.qf1 = self.space.function(dim=self.TNL1) # forward  propagator 
        self.qb1 = self.space.function(dim=self.TNL1) # backward propagator
        self.qf1[0] = 1
        self.qb1[0] = 1
       
        ## propagators of A2B2
        self.qf2 = self.space.function(dim=self.TNL2) # forward  propagator 
        self.qb2 = self.space.function(dim=self.TNL2) # backward propagator

        self.qf2[0] = 1
        self.qb2[0] = 1


        self.rho = self.space.function(dim=options['nspecies'])
        self.grad = self.space.function(dim=options['nspecies']+1)
        self.w = self.space.function(dim=options['nspecies'])
        self.wplus = self.space.function(dim=1)
        self.Q = np.zeros(options['nblend'], dtype=np.float)

        self.box_iter = 0
        self.box = box
        self.box_old = np.zeros((box.shape))

        self.gradH_old = np.zeros((box.shape))
        self.gradH = np.zeros((box.shape))

        self.H = np.inf

        self.iter = 0
        self.count = 0

        self.M= 50
        
        self.d = []
        self.wall = []
        self.S = np.zeros((self.M+1, self.M+1))


       
    def reinit(self, box):
        options = self.options
        self.space = FourierSpace(box,  options['NS'])

        self.pdesolvers1 = []
        for i in range(options['nblock1']):
            self.pdesolvers1.append(
                    ParabolicFourierSolver(self.space, self.timelines1[i])
                    )
        self.pdesolvers2 = []
        for i in range(options['nblock2']):
            self.pdesolvers2.append(
                    ParabolicFourierSolver(self.space, self.timelines2[i])
                    )
   
        ## propagators of A1B1
        self.qf1 = self.space.function(dim=self.TNL1) # forward  propagator 
        self.qb1 = self.space.function(dim=self.TNL1) # backward propagator
        self.qf1[0] = 1
        self.qb1[0] = 1
       
        ## propagators of A2B2
        self.qf2 = self.space.function(dim=self.TNL2) # forward  propagator 
        self.qb2 = self.space.function(dim=self.TNL2) # backward propagator

        self.qf2[0] = 1
        self.qb2[0] = 1

        self.Q = np.zeros(options['nblend'], dtype=np.float)


    def init_field1(self, rho):
        options = self.options
        chiABN = options['chiAB']*options['ndeg']

        self.w[0] = chiABN*rho[1]
        self.w[1] = chiABN*rho[0]

    def init_field(self, w):
        self.w[0] = w[0]
        self.w[1] = w[1]

    def compute(self, e_diff=1):
        """
        目标函数，给定外场，计算哈密尔顿量及其梯度
        """
        # solver the forward and backward equation
        import time
        start1 =time.clock()
        self.compute_propagator()
        end =time.clock()
        print('Running timeq: %s Seconds'%(end-start1))
        start1 =time.clock()
        self.compute_single_Q()
        #self.test_compute_single_Q()
        print("Q:", self.Q)
        end =time.clock()
        print('Running timeQ: %s Seconds'%(end-start1))
        # compute density
        start1 =time.clock()
        self.compute_density()
        self.save_data('./BCC/tmp.mat')
        end =time.clock()
        print('Running time phi: %s Seconds'%(end-start1))

        start1 =time.clock()
        self.update_field()
        #if e_diff > 0:
        #    print('Anderson mixing')
        #    self.update_field_anderson()
        #else:
        #    print('Simple mixing')
        #    self.update_field_simple()
 
        end =time.clock()
        print('Running time_field: %s Seconds'%(end-start1))

        start1 =time.clock()
        self.compute_wplus()
        end =time.clock()
        print('Running time w+: %s Seconds'%(end-start1))

        start1 =time.clock()
        self.compute_energe()
        end =time.clock()
        print('Running time H: %s Seconds'%(end-start1))

        start1 =time.clock()
        self.compute_gradient()
        end =time.clock()
        print('Running time grad: %s Seconds'%(end-start1))

        self.iter +=1


    def update_field(self, alpha=0.05):
        """
        Parameters
        ----------
        """

        w = self.w
        wplus = self.wplus
        rho = self.rho
        options = self.options
        chiABN = options['chiAB']*options['ndeg']
        
        w1_bar = chiABN*rho[1] + wplus - w[0]
        w1 = np.fft.fftn(w1_bar)
        w1[0,0] = 0
        w[0] = np.fft.ifftn(np.fft.fftn(w[0])+alpha*w1).real
        
        w2_bar = chiABN*rho[0] + wplus - w[1]
        w2 = np.fft.fftn(w2_bar)
        w2[0,0] = 0
        w[1] = np.fft.ifftn(np.fft.fftn(w[1])+alpha*w2).real
        
        self.w = w
        w_bar =np.array([w1_bar,w2_bar])
        return self.w, w_bar
    
    def update_field_simple(self):
        M = self.M
        T = np.zeros(self.w.shape)
        D = np.zeros(self.w.shape)
        self.w, w_bar = self.update_field()
        self.wall.append(self.w)
        d = w_bar - self.w
        tmp1 = self.inner(d[0], d[0]) + self.inner(d[1], d[1]) 
        tmp2 = self.inner(self.w[0], self.w[0]) + self.inner(self.w[1],self.w[1])
        self.tol = np.sqrt(tmp1/tmp2)
        self.d.append(d)
        n = self.iter if self.iter < M else M
        self.S = self.SMatrix(self.S, self.d, n)
     
    def update_w_bar(self):
        w_bar = np.zeros(self.w.shape)
        wplus = self.wplus
        rho = self.rho
        options = self.options
        chiABN = options['chiAB']*options['ndeg']
        w_bar[0] = chiABN*rho[1] + wplus
        w_bar[1] = chiABN*rho[0] + wplus
        return w_bar
    
    def update_field_anderson(self, alpha=0.2):

        self.tol = 0
        M = self.M
        T = np.zeros(self.w.shape)
        D = np.zeros(self.w.shape)

        if self.iter < M+10 or self.tol >1e-2:
            print('simple mixing')
            self.update_field_simple()
        else:
            print('anderson mixing')
            w_bar = self.update_w_bar()
            d = w_bar - self.w
            self.d.append(d)
            n = self.iter if self.iter < M else M
            self.S = self.SMatrix(self.S, self.d, n)

            C = self.coff(self.S)
            for i in range(2):
                T[i] = self.w[i] 
                for j in range(M):
                    #print('w', self.wall[-j-2])
                    T[i] += C[j]*(self.wall[-j-2][i][:]-self.w[i]) ###需要场的历史信
            
            for i in range(2):
                D[i] = self.d[-1][i] 
                for j in range(M):
                    D[i] += C[j]*(self.d[-j-2][i][:]-self.d[-1][i]) ###需要场的历史信息


            #T= self.w + np.sum(C[...,np.newaxis,np.newaxis,np.newaxis]*(self.wall[-M-1:-1]-self.w))###需要场的历史信息
            #D = self.d[-1] + np.sum(C[...,np.newaxis,np.newaxis,np.newaxis]*(self.d[-M:-1]-self.d[-1]))

            self.w = T + alpha*D
            self.wall.append(self.w)
    
    def coff(self,S):
        N = S.shape[0]-1
        U = np.zeros((N,N))
        V = np.zeros(N)
        for i in range(N):
            for j in range(N):
                U[i][j] = S[0,0] - S[i+1,0] -S[0,j+1] +S[i+1,j+1]
            V[i] = S[0,0] - S[i+1,0]
        C = np.linalg.pinv(U)@V
        return C


    def inner(self, f, g):
        d = np.fft.ifftn(f*g)
        inn = np.real(d.flat[0])
        return inn
    
    def SMatrix(self, S, d, n):
        S_new = S.copy()
        S_new[1:,1:] = S[:-1,:-1]
        for i in range(n+1):
            S_new[0,i] = self.inner(d[-1][0], d[-1-i][0])
            S_new[0,i] += self.inner(d[-1][1], d[-1-i][1])
        S_new[:,0] = S_new[0,:]
        return S_new

    def compute_wplus(self):
        w = self.w
        options = self.options
        chiABN = options['chiAB']*options['ndeg']

        self.wplus = w[0] + w[1] - chiABN
        self.wplus /=2

    def compute_energe(self):
        options = self.options
        chiABN = options['chiAB']*options['ndeg']
        w = self.w
        wplus = self.wplus
        rho = self.rho
        phi1 = self.phi1
        phi2 = self.phi2
        gamma = self.gamma
        E = chiABN*rho[0]*rho[1] 
        E -= w[0]*rho[0]
        E -= w[1]*rho[1]
        #E -= wplus*(1 - rho.sum(axis=0))
        E = np.fft.ifftn(E)
        self.H = np.real(E.flat[0])
        self.H -= phi1*np.log(self.Q[0]/phi1)
        self.H -= (phi2/gamma)*np.log(self.Q[1]/phi2)

    def compute_gradient(self):
        w = self.w
        wplus = self.wplus
        rho = self.rho
        options = self.options
        chiABN = options['chiAB']*options['ndeg']
        self.grad[0] = rho[0] + rho[1] - 1
        self.grad[1] = w[0] - chiABN*rho[1] - wplus
        self.grad[2] = w[1] - chiABN*rho[0] - wplus
        
    def compute_propagator(self):
        options = self.options
        w = self.w
        qf1 = self.qf1
        qb1 = self.qb1
        
        qf2 = self.qf2
        qb2 = self.qb2

        F = [w[0], w[1]]
        ##A1B1
        start = 0
        for i in range(options['nblock1']):
            NL = self.timelines1[i].number_of_time_levels()
            import time
            start1 =time.clock()
            self.pdesolvers1[i].BDF4(self.qf1[start:start + NL], F[i])
            end =time.clock()
            print('Running time: %s Seconds'%(end-start1))
            start += NL - 1
        #print("qf1", self.qf1)

        start = 0
        for i in range(options['nblock1']-1, -1,-1):
            NL = self.timelines1[i].number_of_time_levels()
            self.pdesolvers1[i].BDF4(self.qb1[start:start + NL], F[i])
            start += NL - 1
        #print("qb1", self.qb1)
        
        ##A2B2
        start = 0
        for i in range(options['nblock2']):
            NL = self.timelines2[i].number_of_time_levels()
            import time
            start1 =time.clock()
            self.pdesolvers2[i].BDF4(self.qf2[start:start + NL], F[i])
            end =time.clock()
            print('Running time: %s Seconds'%(end-start1))
            start += NL - 1
        #print("qf1", self.qf2)

        start = 0
        for i in range(options['nblock2']-1, -1,-1):
            NL = self.timelines2[i].number_of_time_levels()
            self.pdesolvers2[i].BDF4(self.qb2[start:start + NL], F[i])
            start += NL - 1
        #print("qb2", self.qb2)


    def compute_single_Q(self, index=-1):
        q1 = self.qf1[index]
        q1 = np.fft.ifftn(q1)
        self.Q[0] = np.real(q1.flat[0])##Q1
        q2 = self.qf2[index]
        q2 = np.fft.ifftn(q2)
        self.Q[1] = np.real(q2.flat[0])##Q2
        print('QQ',self.Q)
        return self.Q

    def test_compute_single_Q(self):
        q1 = self.qf1*self.qb1[-1::-1]
        q2 = self.qf2*self.qb2[-1::-1]
        Q1 = np.zeros(self.TNL1)
        Q2 = np.zeros(self.TNL2)
        for i in range(self.TNL1):
            qq1 = q1[i]
            qq1 = np.fft.ifftn(qq1)
            #self.Q[0] = np.real(qq1.flat[0])##Q1
            Q1[i] = np.real(qq1.flat[0])##Q1
        
        for i in range(self.TNL2):
            qq2 = q2[i]
            qq2 = np.fft.ifftn(qq2)
            #self.Q[1] = np.real(qq2.flat[0])##Q2
            Q2[i] = np.real(qq2.flat[0])##Q1
        self.Q[0] = np.max(Q1)
        self.Q[1] = np.max(Q2)
    
    def compute_density(self):
        options = self.options
        gamma = self.gamma
        phi1 = self.phi1
        phi2 = self.phi2

        q1 = self.qf1*self.qb1[-1::-1]
        q2 = self.qf2*self.qb2[-1::-1]

        start = 0
        rho = []
        for i in range(options['nblock1']):
            NL = self.timelines1[i].number_of_time_levels()
            dt = self.timelines1[i].current_time_step_length()
            rho.append(self.integral_time(q1[start:start+NL], dt))
            start += NL - 1

        start = 0
        for i in range(options['nblock2']):
            NL = self.timelines2[i].number_of_time_levels()
            dt = self.timelines2[i].current_time_step_length()
            rho.append(self.integral_time(q2[start:start+NL], dt))
            start += NL - 1
 
        self.rho[0] = phi1/self.Q[0]*rho[0] + phi2/(gamma*self.Q[1])*rho[2]
        self.rho[1] = phi1/self.Q[0]*rho[1] + phi2/(gamma*self.Q[1])*rho[3]

        #print("densityA", self.rho[0])
        #print("densityB", self.rho[1])

    def integral_time(self, q, dt):
        f = -0.625*(q[0] + q[-1]) + 1/6*(q[1] + q[-2]) - 1/24*(q[2] + q[-3])
        f += np.sum(q, axis=0)
        f *= dt
        return f
    
    def box_adjust(self, dbox=0.1, dt=0.1):
        print('adjuststep:', self.box_iter)

        box = self.box.copy()
        print('box', box)
        rbox = box.copy()
        lbox = box.copy()
        
        gradH = self.gradH.copy()
        
        for i in range(box.shape[0]):
            for j in range(i+1):
                rbox[i][j] = box[i][j] + dbox
                print('rbox:',rbox)
                self.reinit(rbox)
                self.compute_propagator()
                self.compute_single_Q()
                self.compute_energe()
                H_r = self.H.copy()
                lbox[i][j] = box[i][j] - dbox
                print('lbox', lbox)
                self.reinit(lbox)
                self.compute_propagator()
                self.compute_single_Q()
                self.compute_energe()
                H_l = self.H.copy()
                gradH[i][j] = (H_r-H_l)/(2*dbox)
        print('grad', gradH)

        if self.box_iter == 0:
            dt = dt
        else:
            box_diff = box - self.box_old
            print('boxdiff', box_diff)
            grad_diff = gradH - self.gradH_old
            print('gradHold', self.gradH_old)
            print('graddiff', grad_diff)
            if self.box_iter % 2 == 0:
                dt = np.sum(box_diff*box_diff)/np.sum(box_diff*grad_diff)
            else:
                dt = np.sum(box_diff*grad_diff)/np.sum(grad_diff*grad_diff)
        print('dt', dt)
        
        self.box_old = box.copy()
        self.box = box - dt*gradH
        print('boxnew', self.box)
        
        self.gradH_old = gradH.copy()
        self.gradH = gradH.copy()
        
        self.box_iter += 1
    
    def box_adjust1(self, dbox=0.1, dt=0.1):
        print('adjuststep:', self.box_iter)

        box = self.box.copy()
        print('box', box)
        rbox = box.copy()
        lbox = box.copy()
        
        gradH = self.gradH.copy()
        rbox[0,0] = box[0,0] + dbox
        rbox[1,1] = rbox[0,0] 
        rbox[2,2] = rbox[0,0] 
        print('rbox:',rbox)
        self.reinit(rbox)
        self.compute_propagator()
        self.compute_single_Q()
        self.compute_energe()
        H_r = self.H.copy()
        lbox[0,0] = box[0,0] - dbox
        lbox[1,1] = lbox[0,0] 
        lbox[2,2] = lbox[0,0] 
        print('lbox', lbox)
        self.reinit(lbox)
        self.compute_propagator()
        self.compute_single_Q()
        self.compute_energe()
        H_l = self.H.copy()
        gradH = (H_r-H_l)/(2*dbox)
        print('grad', gradH)
        box_diff = box - self.box_old
        print('boxdiff', box_diff)
        grad_diff = gradH - self.gradH_old
        print('gradHold', self.gradH_old)
        print('graddiff', grad_diff)
 
        if self.box_iter == 0:
            dt = dt
        elif self.iter%2 == 0:
            dt = np.sum(box_diff*box_diff)/np.sum(box_diff*grad_diff)
        else:
            dt = np.sum(box_diff*grad_diff)/np.sum(grad_diff*grad_diff)
        print('dt', dt)
        
        self.box_old = box.copy()
        self.box[0,0] = box[0,0] - dt*gradH
        self.box[1,1] = self.box[0,0]
        self.box[2,2] = self.box[0,0]
        print('boxnew', self.box)
        
        self.reinit(self.box)
        self.compute_propagator()
        self.compute_single_Q()
        self.compute_energe()
        print('H',self.H)

        self.gradH_old = gradH.copy()
        self.gradH = gradH.copy()
        
        self.box_iter += 1
    
    
    def convergence_energy(self):
        options = self.options
        H = self.H
        H_diff = np.inf
        e_diff = 1
        maxit = options['Maxit']
        for i in range(maxit):
            self.compute(e_diff = e_diff)
            print('H:',H)
            H_diff_new = np.abs(H - self.H)
            e_diff = H_diff - H_diff_new
            H_diff = H_diff_new
            print('Hmodel:', self.H)
            print('Hdiff:',H_diff)
            print('ediff:',e_diff)
            H = self.H
            if H_diff < 5e-6:
                if np.max(model.rho[:,0]) < 1 and np.min(model.rho[:,0]) >0:
                        break
        return H

    def save_data(self, fname='rho.mat'):
        import scipy.io as sio

        rhoA = self.rho[0]
        rhoB = self.rho[1]
        H = self.H
        box = self.box
        w = self.w
        qf1 = self.qf1
        qb1 = self.qb1
        qf2 = self.qf2
        qb2 = self.qb2
        data = {
                'rhoA':rhoA,
                'rhoB':rhoB,
                'H':H,
                'box':box,
                'w':w,
                'qf1':qf1,
                'qb1':qb1,
                'qf2':qf2,
                'qb2':qb2,
                }
        sio.savemat(fname, data)
