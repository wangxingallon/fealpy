import numpy as np
import matplotlib.pyplot as plt
import math

from fealpy.functionspace import FourierSpace
from fealpy.timeintegratoralg.timeline import UniformTimeLine

from ParabolicFourierSolver import ParabolicFourierSolver

def model_options(
        nspecies = 2,
        nblend = 2,
        nblock1 = 2,
        nblock2 = 2,
        ndeg = 100,
        fA1 = 0.25,
        fB1 = 0.25,
        fA2 = 0.25,
        fB2 = 0.25,
        chiAB = 0.14,
        box = np.diag(2*[2*np.pi]),
        NS = 256,
        maxdt = 0.01,
        bA = 1,
        bB = 1,
        gamma = 1,
        Maxit = 5000,
        tol = 1e-7):
        # the parameter for scft model
        options = {
                'nspecies': nspecies,
                'nblend': nblend,
                'nblock1': nblock1,
                'nblock2': nblock2,
                'ndeg': ndeg,
                'fA1': fA1,
                'fB1': fB1,
                'fA2': fA2,
                'fB2': fB2,
                'chiAB': chiAB,
                'box': box,
                'dim': len(box),
                'NS' : NS,
                'maxdt': maxdt,
                'bA': bA,
                'bB': bB,
                'gamma': gamma,
                'Maxit':Maxit,
                'tol':tol
                }
        return options


class SCFTA1B1A2B2LinearModel():
    def __init__(self, options=None):
        if options == None:
            options = pscftmodel_options()
        self.options = options
        dim = options['dim']
        box = options['box']

        box_period = 2*np.pi*inv(box.T)
        
        self.space = FourierSpace(box,  options['NS'])

        fA1 = options['fA1']
        fB1  = options['fB1']
        fA2 = options['fA2']
        fB2  = options['fB2']
        maxdt = options['maxdt']
        self.gamma = options['gamma']

        self.phi2 = 0.2
        self.phi1 = 1 - self.phi2

        self.timelines1 = []
        self.timelines1.append(UniformTimeLine(0, fA1, int(np.ceil(fA1/maxdt))))
        self.timelines1.append(UniformTimeLine(0, fB1,  int(np.ceil(fB1/maxdt))))
        self.timelines2 = []
        self.timelines2.append(UniformTimeLine(0, self.gamma*fA2,int(np.ceil(self.gamma*fA2/maxdt))))
        self.timelines2.append(UniformTimeLine(0, self.gamma*fB2,int(np.ceil(self.gamma*fB2/maxdt))))


        self.pdesolvers1 = []
        for i in range(options['nblock1']):
            self.pdesolvers1.append(
                    ParabolicFourierSolver(self.space, self.timelines1[i])
                    )
        self.pdesolvers2 = []
        for i in range(options['nblock2']):
            self.pdesolvers2.append(
                    ParabolicFourierSolver(self.space, self.timelines2[i])
                    )
   
        # total number of time levels of A1B1
        self.TNL1 = 0         
        for i in range(options['nblock1']):
            self.TNL1 += self.timelines1[i].number_of_time_levels()
        self.TNL1 -= options['nblock1'] - 1
        
        # total number of time levels of A2B2
        self.TNL2 = 0
        for i in range(options['nblock2']):
            self.TNL2 += self.timelines2[i].number_of_time_levels()
        self.TNL2 -= options['nblock2'] - 1

        ## propagators of A1B1
        self.qf1 = self.space.function(dim=self.TNL1) # forward  propagator 
        self.qb1 = self.space.function(dim=self.TNL1) # backward propagator
        self.qf1[0] = 1
        self.qb1[0] = 1
       
        ## propagators of A2B2
        self.qf2 = self.space.function(dim=self.TNL2) # forward  propagator 
        self.qb2 = self.space.function(dim=self.TNL2) # backward propagator

        self.qf2[0] = 1
        self.qb2[0] = 1


        self.rho = self.space.function(dim=options['nspecies'])
        self.grad = self.space.function(dim=options['nspecies']+1)
        self.w = self.space.function(dim=options['nspecies'])
        self.wplus = self.space.function(dim=1)
        self.Q = np.zeros(options['nblend'], dtype=np.float)

    def init_field(self, rho):
        options = self.options
        chiABN = options['chiAB']*options['ndeg']

        self.w[0] = chiABN*rho[1]
        self.w[1] = chiABN*rho[0]

    def compute(self):
        """
        目标函数，给定外场，计算哈密尔顿量及其梯度
        """
        # solver the forward and backward equation
        import time
        start1 =time.clock()
        self.compute_propagator()
        end =time.clock()
        print('Running timeq: %s Seconds'%(end-start1))
        start1 =time.clock()
#        self.compute_single_Q()
#        print("Q:", self.Q)
        end =time.clock()
        print('Running timeQ: %s Seconds'%(end-start1))
        # compute density
        start1 =time.clock()
        self.compute_density()
        self.save_data('rho.mat')
        end =time.clock()
        print('Running time phi: %s Seconds'%(end-start1))

        start1 =time.clock()
        self.update_field()
        end =time.clock()
        print('Running time_field: %s Seconds'%(end-start1))

        start1 =time.clock()
        self.compute_wplus()
        end =time.clock()
        print('Running time w+: %s Seconds'%(end-start1))

        start1 =time.clock()
        self.compute_energe()
        end =time.clock()
        print('Running time H: %s Seconds'%(end-start1))

        start1 =time.clock()
        self.compute_gradient()
        end =time.clock()
        print('Running time grad: %s Seconds'%(end-start1))


    def update_field(self, alpha=0.01):
        """
        Parameters
        ----------
        """

        w = self.w
        wplus = self.wplus
        rho = self.rho
        options = self.options
        chiABN = options['chiAB']*options['ndeg']
        
        w1 = chiABN*rho[1] + wplus - w[0]
        w1 = np.fft.fftn(w1)
        w1[0,0] = 0
        w[0] = np.fft.ifftn(np.fft.fftn(w[0])+alpha*w1).real
        
        w2 = chiABN*rho[0] + wplus - w[1]
        w2 = np.fft.fftn(w2)
        w2[0,0] = 0
        w[1] = np.fft.ifftn(np.fft.fftn(w[1])+alpha*w2).real

    def compute_wplus(self):
        w = self.w
        options = self.options
        chiABN = options['chiAB']*options['ndeg']

        self.wplus = w[0] + w[1] - chiABN
        self.wplus /=2

    def compute_energe(self):
        options = self.options
        chiABN = options['chiAB']*options['ndeg']
        w = self.w
        wplus = self.wplus
        rho = self.rho
        phi1 = self.phi1
        phi2 = self.phi2
        gamma = self.gamma
        E = chiABN*rho[0]*rho[1] 
        E -= w[0]*rho[0]
        E -= w[1]*rho[1]
        E -= wplus*(1 - rho.sum(axis=0))
        E = np.fft.ifftn(E)
        self.H = np.real(E.flat[0])
        self.H -= phi1*np.log(self.Q[0]/phi1)
        self.H -= (phi2/gamma)*np.log(self.Q[1]/phi2)

    def compute_gradient(self):
        w = self.w
        wplus = self.wplus
        rho = self.rho
        options = self.options
        chiABN = options['chiAB']*options['ndeg']
        self.grad[0] = rho[0] + rho[1] - 1
        self.grad[1] = w[0] - chiABN*rho[1] - wplus
        self.grad[2] = w[1] - chiABN*rho[0] - wplus

    def compute_propagator(self):
        print('a')

        options = self.options
        w = self.w
        print('w',self.w)
        qf1 = self.qf1
        qb1 = self.qb1
        
        qf2 = self.qf2
        qb2 = self.qb2

        F = [w[0], w[1]]
        ##A1B1
        start = 0
        for i in range(options['nblock1']):
            NL = self.timelines1[i].number_of_time_levels()
            import time
            start1 =time.clock()
            self.pdesolvers1[i].BDF4(self.qf1[start:start + NL], F[i])
            end =time.clock()
            print('Running time: %s Seconds'%(end-start1))
            start += NL - 1
        #print("qf1", self.qf1)

        start = 0
        for i in range(options['nblock1']-1, -1,-1):
            NL = self.timelines1[i].number_of_time_levels()
            self.pdesolvers1[i].BDF4(self.qb1[start:start + NL], F[i])
            start += NL - 1
        #print("qb1", self.qb1)
        
        ##A2B2
        start = 0
        for i in range(options['nblock2']):
            NL = self.timelines2[i].number_of_time_levels()
            import time
            start1 =time.clock()
            self.pdesolvers2[i].BDF4(self.qf2[start:start + NL], F[i])
            end =time.clock()
            print('Running time: %s Seconds'%(end-start1))
            start += NL - 1
        #print("qf", self.qf2[-1])

        start = 0
        for i in range(options['nblock2']-1, -1,-1):
            NL = self.timelines2[i].number_of_time_levels()
            self.pdesolvers2[i].BDF4(self.qb2[start:start + NL], F[i])
            start += NL - 1
        #print("qb", self.qb2[-1])


    def compute_single_Q(self, index=-1):
        q = self.qf1[index]
        q = np.fft.ifftn(q)
        self.Q[0] = np.real(q.flat[0])##Q1
        q = self.qf2[index]
        q = np.fft.ifftn(q)
        self.Q[1] = np.real(q.flat[0])##Q2
        return self.Q

    def test_compute_single_Q(self):
        q1 = self.qf1*self.qb1[-1::-1]
        q2 = self.qf2*self.qb2[-1::-1]
        q = np.zeros(self.TNL2)
        for i in range(self.TNL1):
            qq = q2[i]
            print(qq.shape)
            qq = np.fft.ifftn(qq)
            self.Q[0] = np.real(qq.flat[0])##Q1
            print(self.Q[0])
            
    def compute_density(self):
        options = self.options
        gamma = self.gamma
        phi1 = self.phi1
        phi2 = self.phi2

        q1 = self.qf1*self.qb1[-1::-1]
        q2 = self.qf2*self.qb2[-1::-1]

        start = 0
        rho = []
        for i in range(options['nblock1']):
            NL = self.timelines1[i].number_of_time_levels()
            dt = self.timelines1[i].current_time_step_length()
            rho.append(self.integral_time(q1[start:start+NL], dt))
            start += NL - 1

        start = 0
        for i in range(options['nblock2']):
            NL = self.timelines2[i].number_of_time_levels()
            dt = self.timelines2[i].current_time_step_length()
            rho.append(self.integral_time(q2[start:start+NL], dt))
            start += NL - 1
 
        self.rho[0] = phi1/self.Q[0]*rho[0] + phi2/(gamma*self.Q[1])*rho[2]
        self.rho[1] = phi1/self.Q[0]*rho[1] + phi2/(gamma*self.Q[1])*rho[3]

        #print("densityA", self.rho[0])
        #print("densityB", self.rho[1])

    def integral_time(self, q, dt):
        f = -0.625*(q[0] + q[-1]) + 1/6*(q[1] + q[-2]) - 1/24*(q[2] + q[-3])
        f += np.sum(q, axis=0)
        f *= dt
        return f

    def normalbox(self, box):
        return 2*np.pi*inv(box.T)
    
    def box_adjust(self, dbox=0.1, dt=1):
        self.box_iter += 1

        # box
        box = self.box
        box_old = self.box_old


        # gradbox
        gradbox = self.gradbox
        gradbox_old = self.gradbox_old

        #  difference quotient
        for i in range(box.shape[0]):
            for j in range(i+1):
                rbox = box.copy()
                rbox[i][j] += dbox
                print('rbox:',rbox)
                self.box = self.normalbox(rbox)
                self.reinit()    
                H_r = self.convergence_energy()
                lbox = box.copy()
                lbox[i][j] -= dbox
                self.box = self.normalbox(lbox)
                self.reinit()
                H_l = self.convergence_energy()
                self.gradbox[i][j] = (H_r-H_l)/(2*dbox)
        
        if self.box_iter == 1:
            dt = dt # iteration step length
        else:
            gradbox_diff = gradbox - gradbox_old
            box_diff = box - box_old
            dt = np.sum(box_diff*box_diff)/np.sum(box_diff*gradbox_diff)       
           
        # iteration   
        self.box = box - dt*self.gradbox
        # reverse
        self.box_old = box.copy()
        self.gradbox_old = gradbox.copy()

    def save_data(self, fname='rho.mat'):
        import scipy.io as sio

        rhoA = self.rho[0]
        rhoB = self.rho[1]
        data = {
                'rhoA':rhoA,
                'rhoB':rhoB,
                }
        sio.savemat(fname, data)
