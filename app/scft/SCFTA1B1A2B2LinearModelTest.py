#!/usr/bin/env python3
#

import sys
import math
import time

import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio

# fealpy module
from fealpy.functionspace import FourierSpace
from fealpy.timeintegratoralg.timeline import UniformTimeLine

# scftt module
from SCFTA1B1A2B2LinearModel import SCFTA1B1A2B2LinearModel, model_options, init_value

class SCFTA1BA2CLinearModelTest():

    def __init__(self):
        pass

    def init_value(self):
        NS = 32
        box = np.array([[0.71, 0, 0], [0, 0.71, 0], [0, 0, 0.71]], dtype=np.float)
        space = FourierSpace(box,  NS)
        rhoA = init_value['fcc']
        rhoA = space.fourier_interpolation(rhoA)
        rhoB = 1-rhoA

        print('rhoA:', rhoA)
        print('rhoB:', rhoB)

        import scipy.io as sio

        data = {
                'rhoA':rhoA,
                'rhoB':rhoB,
                }
        sio.savemat('rho0.mat', data)
    def run(self, rdir):
        NS = 128
        L = 4.9843
        #L=2*np.pi/np.sqrt(2)
        box = np.array([[L, 0, 0], [0, L, 0], [0, 0, L]], dtype=np.float)
        #box = np.array([[L, 0], [0, L]], dtype=np.float)
        fA1  = 0.2
        fA2 = 0.3
        gamma = 1.5
        H = np.inf
        H_diff = np.inf
        e_diff = 1

        for chiAB in (0.15,0.175,0.20,0.225,0.25,0.275,0.30):
            for phi2 in (0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6):
                print('chi', chiAB,phi2)
                options = model_options(box=box, NS=NS, fA1=fA1, fA2=fA2,
                        chiAB=chiAB,gamma=gamma, phi2=phi2)
                model = SCFTA1B1A2B2LinearModel(options=options)
                #rhoA = init_value['hex']
                #rhoA = model.space.fourier_interpolation(rhoA)
                #rho = [rhoA, 1-rhoA]
                #model.init_field1(rho)
                #data = sio.loadmat('./ABAB/BCC/BCC.mat')
                #data = sio.loadmat('hex.mat')
                #wA = data['wA'].reshape(NS,NS,NS)
                #wB = data['wB'].reshape(NS,NS,NS)
                #w = np.array([wA, wB])
                #wA = data['phiA'].reshape(NS,NS,NS)
                #wB = data['phiB'].reshape(NS,NS,NS)
                #w = np.array([wA, wB])
                wB = init_value['bccw']
                wB = model.space.fourier_interpolation(wB)
                w =[-wB, wB]
                #w = data['w']
                model.init_field(w)
                maxit = options['Maxit']
                if True:
                    for i in range(maxit):
                        print("step:", i)
                        model.compute(e_diff = 1)
                        print('H:',H)
                        H_diff_new = np.abs(H - model.H)
                        e_diff = H_diff - H_diff_new
                        H_diff = H_diff_new
                        print('Hmodel:', model.H)
                        print('Hdiff:',H_diff)
                        print('ediff:',e_diff)
                        H = model.H
                        if math.isnan(H):
                            break
                        ng = list(map(model.space.function_norm, model.grad))
                        print("l2 norm of grad:", ng)
                        if H_diff < options['tol']:
                        #if H_diff < 1e-10:
                            if np.max(model.rho[:,0]) < 1 and np.min(model.rho[:,0]) >0:
                                model.save_data('./HEX/HEX+chiAB'+str(chiAB)+'phi2'+str(phi2)+'.mat')
                                break
 

    def run1(self, rdir):
        NS = 64
        L = 4.9843
        #L=2*np.pi/np.sqrt(2)
        box = np.array([[L, 0, 0], [0, L, 0], [0, 0, L]], dtype=np.float)
        #box = np.array([[L, 0], [0, L]], dtype=np.float)
        fA1  = 0.2
        fA2 = 0.3
        gamma = 1.5
        options = model_options(box=box, NS=NS, fA1=fA1, fA2=fA2, chiAB=0.25,
                gamma=gamma, phi2=0.2)
        model = SCFTA1B1A2B2LinearModel(options=options)
        #rhoA = init_value['hex']
        #rhoA = model.space.fourier_interpolation(rhoA)
        #rho = [rhoA, 1-rhoA]
        #model.init_field1(rho)
        data = sio.loadmat('phi-jy.mat')
        wA = data['phiA'].reshape(NS,NS,NS)
        wB = data['phiB'].reshape(NS,NS,NS)
        w = np.array([wA, wB])
        #wB = init_value['bccw']
        #wB = model.space.fourier_interpolation(wB)
        #w =[-wB, wB]
        #w = data['u']
        model.init_field(w)
        H = np.inf
        H_diff = np.inf
        e_diff = 1
        maxit = options['Maxit']
        if True:
            for i in range(maxit):
                print("step:", i)
                model.compute(e_diff = 1)
                print('H:',H)
                H_diff_new = np.abs(H - model.H)
                e_diff = H_diff - H_diff_new
                H_diff = H_diff_new
                print('Hmodel:', model.H)
                print('Hdiff:',H_diff)
                print('ediff:',e_diff)
                H = model.H
                ng = list(map(model.space.function_norm, model.grad))
                print("l2 norm of grad:", ng)
                #if H_diff < options['tol']:
                if H_diff < 1e-10:
                    if np.max(model.rho[:,0]) < 1 and np.min(model.rho[:,0]) >0:
                        break
            model.save_data('./BCC/BCCs.mat')
    
    def adjust_box(self, rdir):
        NS = 64
        L = 4.9843
        box = np.array([[L, 0, 0], [0, L, 0], [0, 0, L]], dtype=np.float)
        #box = np.array([[L, 0], [L, 9]], dtype=np.float)
        fA1  = 0.2
        fA2 = 0.3
        gamma = 1.5
        options = model_options(box=box, NS=NS, fA1=fA1, fA2=fA2, chiAB=0.25, gamma = gamma, phi2=0.1)
        model = SCFTA1B1A2B2LinearModel(options=options)
        #rhoA = init_value['hex']
        #rhoA = model.space.fourier_interpolation(rhoA)
        #rho = [rhoA, 1-rhoA]

        #model.init_field(rho)
        ###B0
        #model.convergence_energy()
        #model.save_data('test0.mat')
        #H = model.H.copy()

        data = sio.loadmat('./bcc/jy7-bcc.mat')
        model.rho = [data['rhoA'], data['rhoB']]
        model.w = data['w']
        model.H = data['H']
        H = model.H
 
        j = 0
        i = 0
        print('start adjust..............................................')
        while True:
            ###Bii
            while True:
                print("step:", j)
                model.box_adjust1(dbox=1e-5)
                j += 1
                grad = model.gradH
                eps = np.max(np.abs(grad))
                if eps < 1e-5:
                    break
            ###计算Bi时候的能量
            model.reinit(model.box)
            model.convergence_energy()
            i+=1
            print('i',i)
            model.save_data('./bcc/wx-bcc'+ str(i)+'.mat')
            ###判断两次B之间的能量差
            H_new = model.H.copy()
            print('Hnew', H_new)
            H_diff = np.abs(H_new - H)
            H = H_new
            print('H', H)
            print('Hdiff:',H_diff)
            if H_diff< 1e-06:
                break

    def adjust_box_B(self, rdir):
        NS = 32
        box = np.array([[0.71, 0, 0], [0, 0.71, 0], [0, 0, 0.71]], dtype=np.float)
        fA1  = 0.2
        fA2 = 0.3
        gamma = 1.5
        options = model_options(box=box, NS=NS, fA1=fA1, fA2=fA2,gamma = gamma)
        model = SCFTA1B1A2B2LinearModel(options=options)
        data = scio.loadmat('data32.mat')
        model.rho = [data['rhoA'], data['rhoB']]
        model.w = data['w']
        model.H = data['H']
        H = model.H
        j = 0
        i = 0
        print('start adjust..............................................')
        while True:
            ###Bi
            while True:
                print("step:", j)
                model.box_adjust(dbox=1e-3, dt=0.1)
                j += 1
                grad = model.gradH
                eps = np.max(np.abs(grad))
                print('eps', eps)
                if eps < 5e-3:
                    break
            ###计算Bi时候的能量
            model.reinit(model.box)
            model.convergence_energy()
            i+=1
            print('i',i)
            model.save_data('data'+ str(i)+'.mat')
            ###判断两次B之间的能量差
            H_new = model.H.copy()
            print('Hnew', H_new)
            H_diff = np.abs(H_new - H)
            H = H_new
            print('H', H)
            print('Hdiff:',H_diff)
            if H_diff< 1e-07:
                break


test = SCFTA1BA2CLinearModelTest()
if sys.argv[1] == "init_value":
    test.init_value()
elif sys.argv[1] == "run":
    start =time.clock()
    test.run(rdir='./results/')
    end =time.clock()
    print('Running time: %s Seconds'%(end-start))
elif sys.argv[1] == "adjust_box":
    start =time.clock()
    test.adjust_box(rdir='./results/')
    end =time.clock()
    print('Running time: %s Seconds'%(end-start))
elif sys.argv[1] == "adjust_box_B":
    start =time.clock()
    test.adjust_box_B(rdir='./results/')
    end =time.clock()
    print('Running time: %s Seconds'%(end-start))
