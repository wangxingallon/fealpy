#!/usr/bin/env python3

import sys
import time
import scipy.io as scio

import numpy as np
import matplotlib.pyplot as plt

from fealpy.mesh import HalfEdgeMesh2d, PolygonMesh
from fealpy.opt.saddleoptalg import SteepestDescentAlg

from SCFTVEMModel2d import scftmodel2d_options, SCFTVEMModel2d
from vem2d_problem import halfedgemesh, init_mesh, complex_mesh


class HalfEdgeAVEMTest():
    def __init__(self, mesh, fieldstype, moptions, optoptions):
        print('NN', mesh.number_of_nodes())
        self.optoptions = optoptions
        obj = SCFTVEMModel2d(mesh, options=moptions)
        data = scio.loadmat('resultsgezi/test500.mat')
        mu = data['mu']
        #mu = obj.init_value(fieldstype=fieldstype)
        self.problem = {'objective': obj, 'x0': mu, 'mesh': mesh}


    def uni_run(self):
        problem = self.problem
        options = self.optoptions
        model = problem['objective']
        while True:
            print('chiN',moptions['chiN'])
            optalg = SteepestDescentAlg(problem, options)
            x, f, g, diff = optalg.run()
            problem['x0'] = x
            if (np.max(model.rho[:,0]) < 1) and (np.min(model.rho[:,0]) >0):
                model.save_data(moptions['rdir']+'/'+ str(int(moptions['chiN']))+'.mat')
                moptions['chiN'] +=5
            if moptions['chiN'] >60:
                break



options = {
        'MaxIters': 5000,
        'MaxFunEvals': 5000,
        'NormGradTol': 1e-6,
        'FunValDiff': 1e-6,
        'StepLength': 2,
        'StepTol': 1e-14,
        'Output': True
        }

moptions = scftmodel2d_options(
        nspecies= 2,
        nblend = 1,
        nblock = 2,
        ndeg = 100,
        fA = 0.2,
        chiAB = 0.25,
        dim = 2,
        T0 = 20,
        T1 = 80,
        nupdate = 1,
        order = 1,
        rdir = sys.argv[2])

mesh = complex_mesh(r=0.009, filename = sys.argv[1], n=0)

Halftest = HalfEdgeAVEMTest(mesh, fieldstype=3, moptions=moptions,
        optoptions=options)

Halftest.uni_run()
