import numpy as np

from fealpy.functionspace import FourierSpace
from fealpy.timeintegratoralg.timeline import UniformTimeLine

from ParabolicFourierSolver import ParabolicFourierSolver
 
bcc = (np.array([
            [ 1,    1,     0],
            [-1,    1,     0],
            [-1,   -1,     0],
            [ 1,   -1,     0],
            [ 0,    1,     1],
            [ 0,   -1,     1],
            [ 1,    0,     1],
            [-1,    0,     1]], dtype=np.int),
            np.array([1.0], dtype=np.float)
            )

##time
f = 0.3
maxdt = 0.01
timeline = UniformTimeLine(0, f, int(np.ceil(f/maxdt)))
##space

N = 32
L = 4.5
box = np.array([[L, 0, 0], [0, L, 0], [0, 0, L]], dtype=np.float)
space = FourierSpace(box,  N)

pdesolver = ParabolicFourierSolver(space, timeline)

TNL = timeline.NL 
dt  = timeline.current_time_step_length()
 
q = space.function(dim=TNL)
q[0] = 1

import scipy.io as scio
data = scio.loadmat('data0.mat')
w = data['w'][0]

pdesolver.BDF4(q, w)
print('q',q)


