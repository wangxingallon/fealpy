import numpy as np
import matplotlib.pyplot as plt
import math

from fealpy.functionspace import FourierSpace
from fealpy.timeintegratoralg.timeline import UniformTimeLine

from ParabolicFourierSolver import ParabolicFourierSolver

init_value = {
        "bcc":( 
        np.array([
            [ 1,    1,     0],
            [-1,    1,     0],
            [-1,   -1,     0],
            [ 1,   -1,     0],
            [ 0,    1,     1],
            [ 0,   -1,     1],
            [ 0,   -1,    -1],
            [ 0,    1,    -1],
            [ 1,    0,     1],
            [-1,    0,     1],
            [-1,    0,    -1],
            [ 1,    0,    -1]], dtype=np.int),
            np.array([0.3], dtype=np.float)
            ),
        "hex":( 
        np.array([
            [ 1,     0],
            [-1,     0],
            [ 0,     1],
            [ 0,    -1],
            [ 1,    -1],
            #[ 1,     1],
            #[-1,    -1],
            [-1,     1]], dtype=np.int),
            np.array([0.3], dtype=np.float)
            ),
        "bccw":(
        np.array([
            [ 1,    1,     0],
            [-1,    1,     0],
            [-1,   -1,     0],
            [ 1,   -1,     0],
            [ 0,    1,     1],
            [ 0,   -1,     1],
            [ 0,   -1,    -1],
            [ 0,    1,    -1],
            [ 1,    0,     1],
            [-1,    0,     1],
            [-1,    0,    -1],
            [ 1,    0,    -1]], dtype=np.int),
            np.array([1.0], dtype=np.float)
            ),
        "hexw":( 
        np.array([
            [ 1,     0],
            [-1,     0],
            [ 0,     1],
            [ 0,    -1],
            [ 1,    -1],
            [-1,     1]], dtype=np.int),
            np.array([1.0], dtype=np.float)
            ),
        "LAM":(
	 np.array([[1, 0], [-1, 0]], dtype=np.int),
         np.array([1, 1], dtype=np.complex)
         ),
        }


def model_options(
        nspecies = 2,
        nblend = 1,
        nblock = 2,
        ndeg = 100,
        fA = 0.2,
        chiAB = 0.20,
        box = np.diag(2*[2*np.pi]),
        NS = 256,
        maxdt = 0.01,
        bA = 1,
        bB = 1,
        Maxit = 5000,
        tol = 1e-7):
        # the parameter for scft model
        options = {
                'nspecies': nspecies,
                'nblend': nblend,
                'nblock': nblock,
                'ndeg': ndeg,
                'fA': fA,
                'chiAB': chiAB,
                'box': box,
                'dim': len(box),
                'NS' : NS,
                'maxdt': maxdt,
                'bA': bA,
                'bB': bB,
                'Maxit':Maxit,
                'tol':tol
                }
        return options


class SCFTABModel():
    def __init__(self, options=None):
        if options == None:
            options = pscftmodel_options()
        self.options = options
        dim = options['dim']
        box = options['box']

        self.space = FourierSpace(box,  options['NS'])

        fA = options['fA']
        fB = 1-fA
        maxdt = options['maxdt']

        self.timelines = []
        self.timelines.append(UniformTimeLine(0, fA, int(np.ceil(fA/maxdt))))
        self.timelines.append(UniformTimeLine(0, fB,  int(np.ceil(fB/maxdt))))


        self.pdesolvers = []
        for i in range(options['nblock']):
            self.pdesolvers.append(
                    ParabolicFourierSolver(self.space, self.timelines[i])
                    )
   
        # total number of time levels of A1B1
        self.TNL = 0         
        for i in range(options['nblock']):
            self.TNL += self.timelines[i].number_of_time_levels()
        self.TNL -= options['nblock'] - 1
        
        ## propagators of A1B1
        self.qf = self.space.function(dim=self.TNL) # forward  propagator 
        self.qb = self.space.function(dim=self.TNL) # backward propagator
        self.qf[0] = 1
        self.qb[0] = 1
       
        self.rho = self.space.function(dim=options['nspecies'])
        self.grad = self.space.function(dim=options['nspecies']+1)
        self.w = self.space.function(dim=options['nspecies'])
        self.wplus = self.space.function(dim=1)
        self.Q = np.zeros(options['nblend']-1, dtype=np.float)
        self.H = np.inf
        
        self.box = box
        self.box_iter = 0
        self.box_old = np.zeros((box.shape))

        self.gradH_old = np.zeros((box.shape))
        self.gradH = np.zeros((box.shape))

        self.iter = 0
        self.count = 0

        self.M= 2
        
        self.d = np.zeros((5000, 2 ,options['NS'], options['NS']))
        self.S = np.zeros((self.M, self.M))



    def reinit(self, box):
        options = self.options
        self.space = FourierSpace(box,  options['NS'])

        self.pdesolvers = []
        for i in range(options['nblock']):
            self.pdesolvers.append(
                    ParabolicFourierSolver(self.space, self.timelines[i])
                    )
        ## propagators of A1B1
        self.qf = self.space.function(dim=self.TNL) # forward  propagator 
        self.qb = self.space.function(dim=self.TNL) # backward propagator
        self.qf[0] = 1
        self.qb[0] = 1
       
        self.Q = np.zeros(options['nblend'] -1, dtype=np.float)

    def init_field(self, w):
        self.w[0] = w[0]
        self.w[1] = w[1]
        #print('w',self.w)

    def init_field1(self, rho):
        options = self.options
        chiABN = options['chiAB']*options['ndeg']

        self.w[0] = chiABN*rho[1]
        self.w[1] = chiABN*rho[0]
        #print('w',self.w)

    def compute(self):
        """
        目标函数，给定外场，计算哈密尔顿量及其梯度
        """
        # solver the forward and backward equation
        import time
        start1 =time.clock()
        self.compute_propagator()
        end =time.clock()
        print('Running timeq: %s Seconds'%(end-start1))
        start1 =time.clock()
        self.compute_single_Q()
        print("Q:", self.Q)
        #self.test_compute_single_Q()
        end =time.clock()
        print('Running timeQ: %s Seconds'%(end-start1))
        # compute density
        start1 =time.clock()
        self.compute_density()
        #self.save_data('./hex/data'+ str(self.count)+'.mat')
        self.save_data('./bcc/data.mat')
        end =time.clock()
        print('Running time phi: %s Seconds'%(end-start1))

        start1 =time.clock()
        #self.update_field()
        self.update_field_anderson()
        end =time.clock()
        print('Running time_field: %s Seconds'%(end-start1))

        start1 =time.clock()
        self.compute_wplus()
        end =time.clock()
        print('Running time w+: %s Seconds'%(end-start1))

        start1 =time.clock()
        self.compute_energe()
        end =time.clock()
        print('Running time H: %s Seconds'%(end-start1))

        start1 =time.clock()
        self.compute_gradient()
        end =time.clock()
        print('Running time grad: %s Seconds'%(end-start1))

        self.iter +=1
    
    def update_field(self, alpha=0.1):
        """
        Parameters
        ----------
        """

        w = self.w
        w_bar = np.zeros(self.w.shape)
        wplus = self.wplus
        rho = self.rho
        options = self.options
        chiABN = options['chiAB']*options['ndeg']
        
        w_bar[0] = chiABN*rho[1] + wplus 
        w1 = w_bar[0]- w[0]
        w1 = np.fft.fftn(w1)
        w1[0,0] = 0
        w[0] = np.fft.ifftn(np.fft.fftn(w[0])+alpha*w1).real
        
        w_bar[1] = chiABN*rho[0] + wplus 
        w2 = w_bar[1]- w[1]
        w2 = np.fft.fftn(w2)
        w2[0,0] = 0
        w[1] = np.fft.ifftn(np.fft.fftn(w[1])+alpha*w2).real
        self.w = w
        #print('w',self.w)
        return self.w, w_bar
    
    def update_field_anderson(self, alpha=0.1):

        self.tol = 0
        M = self.M
        d = self.d
        S = self.S

        if self.iter < 2:
            self.w, w_bar = self.update_field()
            print('i',self.iter)
            d[self.iter] = w_bar - self.w
            tmp1 = self.inner(d[self.iter][0], d[self.iter][0]) + self.inner(d[self.iter][1], d[self.iter][1]) 
            tmp2 = self.inner(self.w[0], self.w[0]) + self.inner(self.w[1],self.w[1])
            self.tol = np.sqrt(tmp1/tmp2)
            S = self.SMatrix(S, d, self.iter)
        else:
            print('as')
            self.w, w_bar = self.update_field()
            self.d[self.iter] = w_bar - self.w
            tmp1 = self.inner(d[self.iter][0], d[self.iter][0]) + self.inner(d[self.iter][1], d[self.iter][1]) 
            tmp2 = self.inner(self.w[0], self.w[0]) + self.inner(self.w[1],self.w[1])

            self.tol = np.sqrt(tmp1/tmp2)
            S = self.SMatrix(S, d, self.iter)

            C = self.coff(S)
            
            T[self.iter] = self.w[self.iter] + np.sum(C*(self.w[:self.iter]-self.w[self.iter]))
            D[self.iter] = self.d[self.iter] + np.sum(C*(self.d[:self.iter]-self.d[self.iter]))
            
            self.w = T[self.iter] + alpha*D[self.iter]
        return self.w

    def coff(self,S):
        M = S.shape[0]
        U = np.zeros(M,M)
        V = np.zeros(M)
        for i in range(M):
            for j in range(M):
                U[i][j] = S[0,0] - S[i,0] -S[0,j] +S[i,j]
            V[i] = S[0,0] - S[i,0]
        C = np.linalg.pinv(U)@V
        return C

    def inner(self, f, g):
        d = np.fft.ifftn(f*g)
        inn = np.real(d.flat[0])
        return inn

    def SMatrix(self, S, d, n):
        S_new = S.copy()
        S_new[1:,1:] = S[:-1,:-1]
        for i in range(n):
            S_new[n,i] += self.inner(d[n], d[n-i])
        S_new[:,n] = S_new[n,:]
        S = S_new
        return S

    def compute_wplus(self):
        w = self.w
        options = self.options
        chiABN = options['chiAB']*options['ndeg']

        self.wplus = w[0] + w[1] - chiABN
        self.wplus /=2

    def compute_energe(self):
        options = self.options
        chiABN = options['chiAB']*options['ndeg']
        w = self.w
        wplus = self.wplus
        rho = self.rho
        E = chiABN*rho[0]*rho[1] 
        E -= w[0]*rho[0]
        E -= w[1]*rho[1]
        #E -= wplus*(1 - rho.sum(axis=0))
        E = np.fft.ifftn(E)
        self.H = np.real(E.flat[0])
        self.H -= np.log(self.Q)
        print('HH',self.H)

    def compute_gradient(self):
        w = self.w
        wplus = self.wplus
        rho = self.rho
        options = self.options
        chiABN = options['chiAB']*options['ndeg']
        self.grad[0] = rho[0] + rho[1] - 1
        self.grad[1] = w[0] - chiABN*rho[1] - wplus
        self.grad[2] = w[1] - chiABN*rho[0] - wplus

    def compute_propagator(self):

        options = self.options
        w = self.w
        qf = self.qf
        qb = self.qb
        
        F = [w[0], w[1]]
        ##A1B1
        start = 0
        for i in range(options['nblock']):
            NL = self.timelines[i].number_of_time_levels()
            dt = self.timelines[i].current_time_step_length()
            import time
            start1 =time.clock()
            self.pdesolvers[i].BDF4(self.qf[start:start + NL], F[i])
            end =time.clock()
            print('Running time: %s Seconds'%(end-start1))
            start += NL - 1
        #print("qf1", self.qf[-1])

        start = 0
        for i in range(options['nblock']-1, -1,-1):
            NL = self.timelines[i].number_of_time_levels()
            dt = self.timelines[i].current_time_step_length()
            self.pdesolvers[i].BDF4(self.qb[start:start + NL], F[i])
            end =time.clock()
            start += NL - 1
        #print("qb1", self.qb[-1])
        

    def compute_single_Q(self, index=-1):
        q = self.qf[index]
        q = np.fft.ifftn(q)
        self.Q = np.real(q.flat[0])##Q1
        return self.Q

    def test_compute_single_Q(self):
        q1 = self.qf*self.qb[-1::-1]
        for i in range(self.TNL):
            qq = q1[i]
            qq = np.fft.ifftn(qq)
            self.Q = np.real(qq.flat[0])##Q1
            print(i, self.Q)
            
    def compute_density(self):
        options = self.options
        q = self.qf*self.qb[-1::-1]

        start = 0
        rho = []
        for i in range(options['nblock']):
            NL = self.timelines[i].number_of_time_levels()
            dt = self.timelines[i].current_time_step_length()
            rho.append(self.integral_time(q[start:start+NL], dt))
            start += NL - 1
 
        self.rho[0] = rho[0]
        self.rho[1] = rho[1]
        self.rho /= self.Q 

        #print("densityA", self.rho[0])
        #print("densityB", self.rho[1])

    def integral_time(self, q, dt):
        f = -0.625*(q[0] + q[-1]) + 1/6*(q[1] + q[-2]) - 1/24*(q[2] + q[-3])
        f += np.sum(q, axis=0)
        f *= dt
        return f

    def convergence_energy(self):
        options = self.options
        H = self.H
        for i in range(options['Maxit']):
            self.compute()
            print('Hnew', self.H)
            print('H', H)
            H_diff = np.abs(self.H - H)
            print('Hdiff',H_diff)
            H = self.H
            if H_diff < options['tol']:
                break
        return H
    
    def box_adjust(self, dbox=0.1, dt=0.1):
        print('adjuststep:', self.box_iter)

        box = self.box.copy()
        print('box', box)
        rbox = box.copy()
        lbox = box.copy()
        
        gradH = self.gradH.copy()
        
        for i in range(box.shape[0]):
            for j in range(i+1):
                rbox[i][j] = box[i][j] + dbox
                print('rbox:',rbox)
                self.reinit(rbox)
                self.compute_propagator()
                self.compute_single_Q()
                self.compute_energe()
                H_r = self.H.copy()
                lbox[i][j] = box[i][j] - dbox
                print('lbox', lbox)
                self.reinit(lbox)
                self.compute_propagator()
                self.compute_single_Q()
                self.compute_energe()
                H_l = self.H.copy()
                gradH[i][j] = (H_r-H_l)/(2*dbox)
        print('grad', gradH)

        box_diff = box - self.box_old
        print('boxdiff', box_diff)
        grad_diff = gradH - self.gradH_old
        print('gradHold', self.gradH_old)
        print('graddiff', grad_diff)
        
        if self.box_iter == 0:
            dt = dt
        elif self.iter%2 == 0:
            dt = np.sum(box_diff*box_diff)/np.sum(box_diff*grad_diff)
        else:
            dt = np.sum(box_diff*grad_diff)/np.sum(grad_diff*grad_diff)
        print('dt', dt)
        
        self.box_old = box.copy()
        self.box = box - dt*gradH
        print('boxnew', self.box)
        
        self.gradH_old = gradH.copy()
        self.gradH = gradH.copy()
        
        self.box_iter += 1
        
    def box_adjust1(self, dbox=0.1, dt=0.1):
        print('adjuststep:', self.box_iter)

        box = self.box.copy()
        print('box', box)
        rbox = box.copy()
        lbox = box.copy()
        
        gradH = self.gradH.copy()
        rbox[0,0] = box[0,0] + dbox
        rbox[1,1] = rbox[0,0] 
        rbox[2,2] = rbox[0,0] 
        print('rbox:',rbox)
        self.reinit(rbox)
        self.compute_propagator()
        self.compute_single_Q()
        self.compute_energe()
        H_r = self.H.copy()
        lbox[0,0] = box[0,0] - dbox
        lbox[1,1] = lbox[0,0] 
        lbox[2,2] = lbox[0,0] 
        print('lbox', lbox)
        self.reinit(lbox)
        self.compute_propagator()
        self.compute_single_Q()
        self.compute_energe()
        H_l = self.H.copy()
        gradH = (H_r-H_l)/(2*dbox)
        print('grad', gradH)
        box_diff = box - self.box_old
        print('boxdiff', box_diff)
        grad_diff = gradH - self.gradH_old
        print('gradHold', self.gradH_old)
        print('graddiff', grad_diff)
 
        if self.box_iter == 0:
            dt = dt
        elif self.iter%2 == 0:
            dt = np.sum(box_diff*box_diff)/np.sum(box_diff*grad_diff)
        else:
            dt = np.sum(box_diff*grad_diff)/np.sum(grad_diff*grad_diff)
        print('dt', dt)
        
        self.box_old = box.copy()
        self.box[0,0] = box[0,0] - dt*gradH
        self.box[1,1] = self.box[0,0]
        self.box[2,2] = self.box[0,0]
        print('boxnew', self.box)
        
        self.reinit(self.box)
        self.compute_propagator()
        self.compute_single_Q()
        self.compute_energe()
        print('H',self.H)

        self.gradH_old = gradH.copy()
        self.gradH = gradH.copy()
        
        self.box_iter += 1
 

    
    def save_data(self, fname='rho.mat'):
        import scipy.io as sio

        rhoA = self.rho[0]
        rhoB = self.rho[1]
        H = self.H
        box = self.box
        w = self.w
        data = {
                'rhoA':rhoA,
                'rhoB':rhoB,
                'H':H,
                'box':box,
                'w':w,
                }
        sio.savemat(fname, data)
